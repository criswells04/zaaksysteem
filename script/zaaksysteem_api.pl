#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Term::ReadKey;

use Pod::Usage;
use WWW::Mechanize;
use URI;
use JSON;

use Data::Dumper;

my (%options, $dry, $verbose);
GetOptions(
    \%options,
    'user|u=s',
    'dry',
    'verbose'
) or usage();
usage() unless @ARGV;

$dry++      if defined($options{dry}) && $options{dry};
$verbose++  if defined($options{verbose}) && $options{verbose};

sub usage { pod2usage(1); }

sub error { die('Error: ' . shift . "\n"); }

sub main {
    my ($options, $url)     = @_;

    $url = _check_url_shorthand($url);

    my $password            = _query_for_password($options, $url)
        or error('No password given');

    my ($base, $ua)         = _connect_to_zaaksysteem($password, $options, $url);


    _request_api_call($options, $base, $ua, $url);
}

sub _check_url_shorthand {
    my ($shorthand) = @_;

    my $config = $ENV{HOME} . '/.zaaksysteem.cfg';
    my ($alias, $url);

    return $shorthand unless($shorthand =~ m[^\w+/]);
    ($alias) = $shorthand =~ m[^(\w+)/];

    if (-f $config) {
        open (my $fh, '<' . $config) or
            error('Could not open ' . $config);

        while (<$fh>) {
            ($url) = $_ =~ m[^(.*?);$alias;];

            if($url) {
                $shorthand =~ s/^$alias/http:\/\/$url/;
                return $shorthand;
            }
        }
    }
}

sub _request_api_call {
    my ($options, $base, $ua, $url)    = @_;

    my %options;
    my $debug_query_string;
    for my $option (@ARGV) {
        next unless $option =~ /=/;

        my ($key, $val) = $option =~ /^(.*?)=(.*?)$/;

        if ($debug_query_string) {
            $debug_query_string .= '&';
        } else {
            $debug_query_string .= '?';
        }

        $debug_query_string .= "$key=$val";

        $options{$key} = $val;
    }

    my $uri = URI->new($url);

    debug('---------- REQUEST --------') if $verbose;
    debug('URL      : ' . $url) if $verbose;
    debug("Options  :\n" . Dumper(\%options))
        if scalar(%options) && $verbose;
    debug('---------------------------') if $verbose;
    debug("JSON:\n" . JSON->new->pretty->encode(\%options)) if $verbose;
    debug('---------------------------') if $verbose;

    my $result = $ua->post(
        $url,
        \%options
    );

    unless ($verbose) {
        print "\nhttp://zs.tld" . $uri->path . $debug_query_string . "\n\n";
    }


    if ($result->header('content-type') =~ /json/) {
        my $perlfromjson = from_json($result->content);
        debug('--------- RESPONSE --------') if $verbose;
        print Dumper($perlfromjson) . "\n" if $verbose;
        debug('---------------------------') if $verbose;
        print JSON->new->pretty->encode($perlfromjson) . "\n";
        debug('---------------------------') if $verbose;
    }

    my $ok;
    if ($result->code =~ /200|201/) {
        $ok++;
        print "OK[" . $result->code . ']' . "\n";
    } else {
        print "FAILED[" . $result->code . "\n";
    }

    return $ok;
}

sub _query_for_password {
    my ($options, $url)     = @_;
    my $config = $ENV{HOME} . '/.zaaksysteem.cfg';

    my ($user, $password);
    if (-f $config) {
        open (my $fh, '<' . $config) or
            error('Could not open ' . $config);

        my $hostport = URI->new($url)->host_port;

        while (<$fh>) {
            next unless $_ =~ /$hostport/;

            ($user, $password) = $_ =~ /^.*?;.*?;(.*?);(.*?)$/;
        }
    }

    if (
        !$user &&
        defined($options->{user}) &&
        $options->{user}
    ) {
        $user = $options->{user};
    }

    error('No user given') unless $user;

    $options->{user} = $user;

    unless ($password) {
        print "Please enter password for '" . $options->{user} . "': ";
        ReadMode('noecho');
        $password    = ReadLine(0);
        chomp($password);
        ReadMode(0);
        print "\n";
    }

    return $password;
}

sub debug {
    my $msg = shift;
    return unless $msg;

    print 'DEBUG: ' . $msg . "\n";
}

sub _connect_to_zaaksysteem {
    my ($password, $options, $url)     = @_;

    my $ua      = WWW::Mechanize->new(
        autocheck   => 0,
        onwarn      => \&debug,
    );

    my $uri     = URI->new( $url );

    my $base    = (
        $uri->secure
            ? 'https://'
            : 'http://'
    ) . $uri->host_port;

    $ua->get( $base . '/auth/login');
    $ua->submit_form(
        fields  => {
            referer     => $base . '/beheer/import',
            username    => $options->{user},
            password    => $password,
        }
    );

    return ($base, $ua);
}

my $url = shift(@ARGV);
main(\%options, $url);

1;

=head1 NAME

zaaksysteem_api.pl - Trigger a zaaksysteem JSON call from the commandline

=head1 SYNOPSIS

 zaaksysteem_api.pl [--dry --verbose] -u|--user=admin URL option=value

 Options
    --user|-u       Username to connect with to zaaksysteem [default: admin]

    --dry           Dry run, do not commit anything
    --verbose       More verbose information about the call

 Config
 When you would like to prevent pounding in your password everytime, please
 put a .zaaksysteem.cfg in your home dir. Containing:

 host:port;shorthand;username;password

 E.g:
 test.zaaksysteem.nl:80;dev;beheerder;gekp@ssword

 MAKE SURE THE FILE IS MODE 600

  # touch ~/.zaaksysteem.cfg
  # chmod 600 ~/.zaaksysteem.cfg

  Example:

  # zaaksysteem_api.pl -u admin http://test.zaaksysteem.nl/event/list scope=bla

=cut



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

