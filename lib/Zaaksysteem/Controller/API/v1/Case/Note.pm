package Zaaksysteem::Controller::API::v1::Case::Note;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Note - APIv1 controller for case note objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case/[CASE_UUID]/note>. Extensive
documentation about this API can be found in:

L<Zaaksysteem::Manual::API::V1::Case::Note>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case::Note>

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::Case::Note;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/note> routing namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('note') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{notes_rs} = $c->model('Object')->rs->search(
        {
            'me.object_class' => 'case/note',
            'object_relation_object_ids.object_uuid' =>
                $c->stash->{zaak}->get_column('uuid'),
        },
        { join => 'object_relation_object_ids' }
    );
}

=head2 instance_base

Reserves the C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $note_uuid) = @_;

    my $note_row = $c->stash->{notes_rs}->find($note_uuid);

    if (!$note_row) {
        throw('api/v1/case/note/not_found', sprintf(
            'No case/note found with UUID "%s" for case "%s"',
            $note_uuid,
            $c->stash->{case}->uuid,
        ));
    }

    $c->stash->{note} = $c->model('Object')->inflate_from_row($note_row);
}

=head2 create

Create a new case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $uuid = $c->stash->{zaak}->get_column('uuid');
    my $case = $c->stash->{zaak};

    $c->stash->{note} = try {
        $c->model('DB')->txn_do(sub {
            my $note = Zaaksysteem::Object::Types::Case::Note->new(
                content  => $c->req->params->{content},
                owner_id => $c->user->uuid,
            );

            my $saved = $c->model('Object')->save_object(object => $note);
            $c->model('DB::ObjectRelation')->create(
                {
                    name           => 'case',
                    object_type    => 'case',
                    object_uuid    => $uuid,
                    object_id      => $saved->id,
                    object_preview =>
                        sprintf("%d %s", $case->id, $case->get_casetype_title),
                }
            );

            return $saved;
        });
    } catch {
        $self->log->info($_);
        throw('api/v1/case/note/create/fault', sprintf(
            'Error creating case/note for case "%s": %s',
            $uuid,
            $_
        ));
    };

    $c->forward('get');
}

=head2 get

Retrieve a single note by its identifier (UUID).

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ note };
}

=head2 update

Update a case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]/update>

=cut

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    try {
        $c->model('DB')->txn_do(sub {
            $c->stash->{note}->content($c->req->params->{content});

            my $saved_note = $c->model('Object')->save_object(object => $c->stash->{note});
            $c->stash->{note} = $saved_note;
        });
    } catch {
        throw('api/v1/case/note/update/fault', sprintf(
            'Error updating note "%s": %s',
            $c->stash->{note}->id,
            $_
        ));
    };

    $c->forward('get');
}

=head2 delete

Remove a case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    try {
        $c->model('DB')->txn_do(sub {
            $c->model('Object')->delete(object => $c->stash->{note});
        });
    } catch {
        throw('api/v1/case/note/delete/fault', sprintf(
            'Error deleting note "%s": %s',
            $c->stash->{note}->id,
            $_
        ));
    };

    $c->forward('list');
}

=head2 list

Retrieve a list of all case note objects for a specific case.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    my $set = Zaaksysteem::API::v1::Set->new(
        iterator => Zaaksysteem::Object::Iterator->new(
            rs       => $c->stash->{notes_rs},
            inflator => sub { $c->model('Object')->inflate_from_row(shift) },
        ),
    )->init_paging($c->request);

    $c->stash->{result} = $set;
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
