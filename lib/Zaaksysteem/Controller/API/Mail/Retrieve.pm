package Zaaksysteem::Controller::API::Mail::Retrieve;
use Zaaksysteem::Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Mail::Retrieve - API to make ZS retrieve email

=head1 DESCRIPTION

Retrieve email from all configured POP3 mailboxes and return the number of messages processed.

=cut

=head1 ACTIONS

=head2 retrieve

/api/mail/retrieve

=cut

sub retrieve :Chained('/api/base') : PathPart('mail/retrieve') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission(qw[beheer]);

    my @interfaces = $c->model('DB::Interface')->search_active(
        { module => 'pop3client' }
    )->all;

    my $message_counter = 0;
    for my $interface (@interfaces) {
        my $model = $interface->model;

        $model->login();
        my $messages = $interface->model->messages();

        while (my $message = $messages->()) {
            $message_counter++;
            $c->stash->{message} = $message->retrieve()
                or next;

            my $success = $c->forward('/api/mail/intake');

            if ($success) {
                $self->log->trace("Message processed successfully. Removing from server.");
                $message->delete();
            } else {
                $self->log->info("Message NOT processed successfully. Not removing from server.");
            }
        }

        $model->close();
    }

    $c->stash->{zapi} = [{"total_messages" => $message_counter}];
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
