package Zaaksysteem::DB::ResultSet::ZaaktypeObjectMutation;
use Zaaksysteem::Moose;

extends qw(
    DBIx::Class::ResultSet
    Zaaksysteem::Zaaktypen::BaseResultSet
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
