package Zaaksysteem::DB::Component::Logging::Subject::Message::Delete;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my %translated_message_type = (
            note => 'notitie',
            contact_moment => 'contactmoment'
    );
    if (exists($translated_message_type{$self->data->{ message_type }})) {
        return sprintf('%s heeft %s verwijderd',
            $self->data->{ username }, $translated_message_type{$self->data->{ message_type }}
        );
    }

    sprintf('%s heeft bericht van type %s verwijderd',
        $self->data->{ username },
        $self->data->{ message_type }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

