package Zaaksysteem::DB::Component::Logging::Case::Object;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use HTML::Entities qw(encode_entities);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Object - 'Base' role for object
interaction events from case content.

=head1 METHODS

=head2 event_category

This method overrides L<Zaaksysteem::DB::Component::Logging/event_category>
and hardcodes the return value to C<object-mutation>.

=cut

sub event_category { 'object-mutation' }

=head2 object_url

This method uses data from the event to attempt to construct an html string
with a hyperlink to the object found therein.

=cut

sub object_url {
    my $self = shift;

    my $uuid = $self->get_column('object_uuid');

    unless($uuid) {
        return sprintf(
            '<span style="text-decoration: line-through" title="Object is niet meer beschikbaar">%s</span>',
            encode_entities($self->data->{ object_label })
        );
    }

    return sprintf(
        '<a target="_top" href="/object/%s">%s</a>',
        $self->get_column('object_uuid'),
        encode_entities($self->data->{ object_label })
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
