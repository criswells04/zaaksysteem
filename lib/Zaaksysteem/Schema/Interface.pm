use utf8;
package Zaaksysteem::Schema::Interface;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Interface

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<interface>

=cut

__PACKAGE__->table("interface");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'interface_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 active

  data_type: 'boolean'
  is_nullable: 0

=head2 case_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 max_retries

  data_type: 'integer'
  is_nullable: 0

=head2 interface_config

  data_type: 'text'
  is_nullable: 0

=head2 multiple

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 module

  data_type: 'text'
  is_nullable: 0

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 objecttype_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 internal_config

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "interface_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "active",
  { data_type => "boolean", is_nullable => 0 },
  "case_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "max_retries",
  { data_type => "integer", is_nullable => 0 },
  "interface_config",
  { data_type => "text", is_nullable => 0 },
  "multiple",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "module",
  { data_type => "text", is_nullable => 0 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "objecttype_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "internal_config",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->has_many(
  "bibliotheek_sjablonens",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { "foreign.interface_id" => "self.id" },
  undef,
);

=head2 case_type_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "case_type_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "case_type_id" },
);

=head2 object_subscription_config_interface_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectSubscription>

=cut

__PACKAGE__->has_many(
  "object_subscription_config_interface_ids",
  "Zaaksysteem::Schema::ObjectSubscription",
  { "foreign.config_interface_id" => "self.id" },
  undef,
);

=head2 object_subscription_interface_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectSubscription>

=cut

__PACKAGE__->has_many(
  "object_subscription_interface_ids",
  "Zaaksysteem::Schema::ObjectSubscription",
  { "foreign.interface_id" => "self.id" },
  undef,
);

=head2 objecttype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "objecttype_id",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "objecttype_id" },
);

=head2 transactions

Type: has_many

Related object: L<Zaaksysteem::Schema::Transaction>

=cut

__PACKAGE__->has_many(
  "transactions",
  "Zaaksysteem::Schema::Transaction",
  { "foreign.interface_id" => "self.id" },
  undef,
);

=head2 user_entities

Type: has_many

Related object: L<Zaaksysteem::Schema::UserEntity>

=cut

__PACKAGE__->has_many(
  "user_entities",
  "Zaaksysteem::Schema::UserEntity",
  { "foreign.source_interface_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-11-01 08:17:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:eHTLiUBXfHXNU2PcE0PVBQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::Interface::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::Interface::Component
    +Zaaksysteem::Helper::ToJSON
/);

__PACKAGE__->inflate_column(
    'internal_config', {
        inflate => sub { JSON::XS->new->decode(shift // '{}') },
        deflate => sub { JSON::XS->new->encode(shift // {}) },
    }
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

