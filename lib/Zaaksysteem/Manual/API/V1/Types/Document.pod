=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Document - Type definition for C<document> objects

=head1 DESCRIPTION

This page documents the serialization of
L<C<document>|Zaaksysteem::Object::Types::Document> object instances.

A document is an object that represents a version of a file with the
associated L<storage|Zaaksysteem::Object::Types::File> and
L<metadata|Zaaksysteem::Object::Types::Document::Metadata>.

=head1 JSON

=begin javascript

{
   "instance" : {
      "case" : {
         "instance" : null,
         "reference" : "8c79ac41-76b8-43fe-b45f-99dcb684159c",
         "type" : "case"
      },
      "date_created" : "2018-01-30T14:14:41Z",
      "date_modified" : "2018-01-30T14:14:41Z",
      "file" : {
         "instance" : {
            "archivable" : true,
            "date_created" : "2017-12-11T09:43:12Z",
            "date_modified" : "2018-01-30T14:14:41Z",
            "md5" : "b4a7db18b8110369d2c31a8cd922a5c1",
            "mimetype" : "text/plain",
            "name" : "README.md",
            "size" : 13499,
            "virus_scan_status" : "ok"
         },
         "reference" : "b265808e-de57-11e7-ae18-d1e2cbf6af33",
         "type" : "file"
      },
      "filename" : "Example markdown for documentation purposes.md",
      "metadata" : {
         "instance" : {
            "appearance" : "Verschijningsvorm",
            "category" : "Notitie",
            "date_created" : "2018-01-30T14:14:41Z",
            "date_modified" : "2018-01-30T14:14:41Z",
            "description" : "This is the description of a file",
            "origin" : "Intern",
            "origin_date" : "2018-01-10T00:00:00Z",
            "pronom_format" : "fmt/foo/bar",
            "structure" : "Structuur",
            "trust_level" : "Openbaar"
         },
         "reference" : null,
         "type" : "document/metadata"
      },
      "name" : "Example markdown for documentation purposes",
      "number" : 19,
      "version" : 8
   },
   "reference" : null,
   "type" : "document"
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 name E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

This is the name of the document without the extension and may not be filesystem safe

=head2 filename E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Stores the name of the document object. This will usually be the filename
provided during upload of the file, and should be expected to be
filesystem-safe.

=head2 number E<raquo> L<C<serial>|Zaaksysteem::Manual::API::V1::ValueTypes/serial>

This is the sequential identifier for the document

=head2 version E<raquo> L<C<serial_number>|Zaaksysteem::Manual::API::V1::ValueTypes/serial_number>

The current version of the document

=head2 case E<raquo> L<C<reference>|Zaaksysteem::Manual::API::V1::ValueTypes/reference>

Reference to a case object

=head2 file E<raquo> L<C<File>|Zaaksysteem::Manual::API::V1::Types::File>

A file object

=head2 metadata E<raquo> L<C<Metadata>|Zaaksysteem::Manual::API::V1::Type::Document::Metadata>

A file object

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
