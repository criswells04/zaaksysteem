=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Message - Type definition for status
objects

=head1 DESCRIPTION

This page documents the serialization for
C<status|Zaaksysteem::API::v1::Message> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "message",
    "instance": {
        "type": ...,
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 type

References a child of the C<message> type.

=head1 SUBTYPES

Messages are categorized in subtypes which indicate the intent of the message.

In OOP terms, these subtypes are classes inheriting from the main C<message>
type.

=head2 ack

The C<ack> message, or acknowledgement, is a message to indicate a
non-specific OK status after processing a request.

=begin javascript

{
    "reference": null,
    "type": "message",
    "instance": {
        "type": "ack",
        "message": "Your request was received and processed"
    }
}

=end javascript

=head2 phase_transition

The C<phase_transition> message subtype encapsulates the triggering of a phase
transition on a case object.

B<Deprecation warning>: this subtype breaks domain, and will be removed when
generic event processing for objects becomes available in the v1 API.

=begin javascript

{
    "reference": null,
    "type": "message",
    "instance": {
        "type": "phase_transition",
        "case_url": "https://uri.to/case/",
        "case": {
            "type": "case",
            "reference": "b2ee5cdb-3b67-4d7f-b784-446742053ba4",
            "instance": null
        }
    }
}

=end javascript

=head2 ping

The C<ping> message subtype is used in the API readiness checks, and can be
sent by a client in an attempt to verify the API is online and ready for
requests.

=begin javascript

{
    "reference": null,
    "type": "message",
    "instance": {
        "type": "ping",
        "payload": "23FA31B9"
    }
}

=end javascript

=head2 pong

The C<pong> message subtype is used in the API readiness checks, and is
expected as a response to C<ping> messages.

=begin javascript

{
    "reference": null,
    "type": "message",
    "instance": {
        "type": "pong",
        "payload": "23FA31B9"
    }
}

=end javascript

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
