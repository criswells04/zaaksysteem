package Zaaksysteem::TraitFor::Request::Params;

use namespace::autoclean;
use Moose::Role;

use Clone qw(clone);
use JSON::XS;
use Data::Visitor::Callback;
use BTTW::Tools;
use IO::All;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::TraitFor::Request::Params - Decode C<application/json> HTTP
request bodies

=head1 DESCRIPTION

This trait augments L<Catalyst::Request> by looking for a request body
and the C<application/json> content-type from the request headers. If found,
the body is parsed as JSON, and expected to be a JSON object at the root
level. The key-value-pairs from this object will be added to the POST and GET
request parameters.

POST-ed JSON requests will be flattened before being merged with the other
request parameters so that L<JSON/decode_json>'s objects for C<true> and
C<false> values are cast to integer true-ish/false-ish values. The unmodified,
decoded JSON document can be accessed via L</json_body>.

=head1 ATTRIBUTES

=head2 json_body

This attribute will hold a reference to the unmodified JSON request body. A
predicate is available via C<has_json_body>.

=cut

has json_body => (
    is => 'rw',
    predicate => 'has_json_body',
);

=head2 json_parameters

This attribute holds a reference to the modified and decoded JSON request
body.

=cut

has json_parameters => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { return {}; },
);

=head1 METHODS

=head2 parameters

=cut

after parameters => sub {
    my $self = shift;

    my ($content_type, $charset) = $self->headers->content_type;

    # Handle JSON in the body
    if ($self->_body && $content_type eq "application/json") {
        my $request_body = io($self->_body->body->filename)->slurp();

        if (uc($self->method) eq 'GET') {
            throw("request/method/get/request_body", "GET does not support a request body");
        }

        my $decoded = try {
            JSON->new->utf8(1)->decode($request_body);
        }
        catch {
            $self->log->error("Unable to JSON decode '$request_body': $_");
            throw('json/decode', $_);
        };

        # Stash a copy of the bare request body (so the visitor blow doesn't
        # clobber the JSON::Boolean values into integers)
        $self->json_body(clone $decoded);

        # Play whack-a-JSON::XS::Boolean, walk the hash recursively, beat the objects into submission
        ### We're now working around this behavior via the json_body attribute,
        ### but a lot of code relies on this behavior.
        ### TODO Remove dependencies and this behavior.
        my $visitor = Data::Visitor::Callback->new(
            'JSON::XS::Boolean' => sub { $_ = int }
        );

        $visitor->visit($decoded);

        # Clone JSON parameters, so actions can decide on dealing with the
        # combined or json-only parameters.
        $self->json_parameters({ %{ $decoded } });

        # Add it to the parameters hash
        $self->{ parameters } = { %{ $self->{ parameters } }, %{ $decoded } };

        if ($ENV{ZS_DEBUG_JSON}) {
            $self->log->trace("Parameters hash, JSON only: " . dump_terse($decoded));
            $self->log->trace("Parameters hash, including JSON: " . dump_terse($self->{parameters}));
        }
    }
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
