package Zaaksysteem::Filestore::Engine::S3;
use Moose;
use namespace::autoclean;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Filestore::Engine
);

use BTTW::Tools;
use File::Temp qw(:seekable);
use List::Util qw(none);
use Zaaksysteem::External::S3;
use Zaaksysteem::StatsD;

=head1 NAME

Zaaksysteem::Filestore::Engine::S3 - File store backend for Amazon S3 (& compatible)

=head1 DESCRIPTION

This file store engine writes and reads files to S3.

=head1 ATTRIBUTES

=head2 access_key

Access key to use to access S3.

=cut

has access_key => (
    is => 'ro',
    isa => 'Str',
    required => 0,
);

=head2 secret_key

Secret key to use to access S3.

=cut

has secret_key => (
    is => 'ro',
    isa => 'Str',
    required => 0,
);

=head2 authorization_method 

The class to use for signing requests.

=cut

has authorization_method => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    default => 'Net::Amazon::S3::Signature::V2',
);

=head2 use_iam_role

Use IAM role to get access/secret keys.

=cut

has use_iam_role => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 0,
);

=head2 host [optional]

Host (hostname + port only!) to connect to. Defaults to 's3.amazonaws.com'.

=cut

has host => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    predicate => 'has_host',
);

=head2 region [optional]

S3 region to use for signing requests.

=cut

has region => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    default => 'eu-central-1',
);

=head2 sts_region [optional]

STS region to use for AssumeRoleWebIdentity tokens.

=cut

has sts_region => (
    is => 'ro',
    isa => 'Str',
    required => 0,
);

=head2 secure [optional]

Use https if true. Defaults to true.

=cut

has secure => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 1,
);

=head2 use_virtual_host [optional]

Use virtual-host style (xxx.s3.amazonaws.com) instead of path style.

Amazon is deprecating path-style for new buckets, so this is mostly for
development (docker-compose does not support wildcard DNS currently)

Defaults to true.

=cut

has use_virtual_host => (
    is => 'ro',
    isa => 'Bool',
    required => 0,
    default => 0,
);

=head2 location [optional]

Location to create buckets in.

When not specified, defaults to the default of L<Net::Amazon::S3>.

=cut

has location => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    predicate => 'has_location',
);

=head2 bucket [required]

Name of the bucket to store files in.

=cut

has bucket => (
    is => 'ro',
    isa => 'Str',
    required => 1,
);

=head2 storage_bucket [required]

Name of the bucket to use for the currently active customer.

The name is a "legacy" of Swift days, when we stored files in a bucket per customer.

=cut

has storage_bucket => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 s3

A L<Net::Amazon::S3> instance to use. Supplying this is not required, as it's
built lazily from the configuration attributes as needed.

=cut

has s3 => (
    is      => 'ro',
    isa     => 'Net::Amazon::S3',
    lazy    => 1,
    builder => '_build_s3',
);

=head1 METHODS

=head2 download_url(uuid)

Returns a URL that can be used with the C<X-Accel-Redirect> header in Nginx to
offload file downloads to the web server.

To make this work, requires a configuration similar to the following in Nginx:

    location ~ ^/download/s3/(http|https)/(.*?)/(.*) {
        internal;

        set $download_protocol $1;
        set $download_host $2;
        set $download_uri $3;

        proxy_set_header Host $download_host;
        proxy_set_header Authorization '';

        proxy_pass $download_protocol://$download_host/$download_uri;

        # Zaaksysteem determines (and hence overrides) the Content-Type.
        proxy_hide_header Content-Type;

        # Disable partial downloads -- it's all or nothing. This prevents the
        # PDF viewer from requesting the same file over and over.
        proxy_hide_header Accept-Ranges;

        # Replace with your DNS resolver (that can resolve S3 hosts)
        resolver 8.8.8.8;
    }

=cut

sub download_url {
    my $self = shift;
    my $uuid = shift;
    $uuid = lc($uuid);

    my $bucket = $self->_get_bucket();

    my $download_url = $bucket->query_string_authentication_uri(
        $self->_get_filename($uuid), time + 60
    );

    $self->log->trace("Presigned URL for S3 download: $download_url");

    return Zaaksysteem::External::S3->transform_download_url($download_url);
}

=head2 get_path(uuid)

Returns a L<File::Temp> instance with a local cache copy of the stored file
(which stringifies to a file name).

=cut

sub get_path {
    my $self = shift;
    return $self->get_fh(@_);
}

=head2 get_fh(uuid)

Returns a L<File::Temp> instance with a local cache copy of the stored file
(which also works as a file handle).

=cut

sub get_fh {
    my $self = shift;
    my $uuid = lc(shift);

    my $s3_filename = $self->_get_filename($uuid);

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $tmp = File::Temp->new();
    $self->log->trace(sprintf("Retrieving object '%s' in temporary file '%s'", $uuid, $tmp));

    my $bucket = $self->_get_bucket();
    my $meta = $bucket->get_key_filename(
        $s3_filename,
        "GET",
        "$tmp",
    );
    if (not defined $meta) {
        throw(
            "filestore/s3/not_found",
            sprintf("Key '%s' not found in bucket '%s'", $s3_filename, $self->bucket),
        );
    }

    # Reset internal file position
    $tmp->seek(0, SEEK_SET);

    Zaaksysteem::StatsD->statsd->end('filestorage_read_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestorage_read_number', 1);

    return $tmp;
}

=head2 write($uuid, $fh)

Write an object to S3.

=cut

sub write {
    my $self = shift;
    my $uuid = lc(shift);
    my $fh = shift; # ignored
    my $filename = shift;

    my $s3_filename = $self->_get_filename($uuid);

    $self->log->trace(sprintf(
        "Writing new key '%s' to bucket '%s'",
        $s3_filename,
        $self->bucket,
    ));

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $bucket = $self->_get_bucket();
    my $result = $bucket->add_key_filename(
        $s3_filename, "$filename", {
            "content_type" => "application/octet-stream",
            # Potentially for server-side crypto:
            #encryption => 'AES256'
        }
    );
    if (!$result) {
        $self->log->error(sprintf(
            "Upload of '%s' to S3 bucket '%s' failed: %s: %s",
            $s3_filename,
            $self->bucket,
            $bucket->err,
            $bucket->errstr,
        ));

        throw(
            "filestore/s3/write_failed",
            sprintf(
                "Upload of '%s' to S3 bucket '%s' failed",
                $s3_filename,
                $self->bucket
            )
        );
    }

    Zaaksysteem::StatsD->statsd->end('filestorage_write_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestorage_write_number', 1);

    return $uuid;
}

=head2 erase($uuid)

Erases a file from the FileStore or Bucket

=cut

sub erase {
    my $self = shift;
    my $uuid = lc(shift);

    my $s3_filename = $self->_get_filename($uuid);

    $self->log->trace(sprintf(
        "Erasing '%s' from S3 bucket '%s'",
        $s3_filename,
        $self->bucket,
    ));

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $bucket = $self->_get_bucket();
    my $result = $bucket->delete_key($s3_filename);
    if (!$result) {
        $self->log->error(sprintf(
            "Erasing of '%s' to S3 bucket '%s' failed: %s: %s",
            $s3_filename,
            $self->bucket,
            $bucket->err,
            $bucket->errstr,
        ));

        throw(
            "filestore/s3/erase",
            sprintf(
                "Erasing of '%s' to S3 bucket '%s'",
                $s3_filename,
                $self->bucket
            )
        );
    }

    Zaaksysteem::StatsD->statsd->end('filestorage_erase_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestorage_erase_number', 1);
}

=head2 _build_s3

Build a new L<Net::Amazon::S3> instance using the configuration attributes.

=cut

sub _build_s3 {
    my $self = shift;

    my $s3 = Zaaksysteem::External::S3->build_s3(
        host => $self->host,
        secure => $self->secure,
        use_virtual_host => $self->use_virtual_host,
        authorization_method => $self->authorization_method,
        region => $self->region,
        sts_region => $self->sts_region,
        access_key => $self->access_key,
        secret_key => $self->secret_key,
        use_iam_role => $self->use_iam_role,
    );

    return $s3;
}

sub _get_bucket {
    my $self = shift;
    # We don't need the "bucket exists" check here.
    # If the bucket doesn't exist, creating it doesn't help now.
    return $self->s3->bucket($self->bucket);
}

sub _get_filename {
    my $self = shift;
    my $filename = shift;

    return sprintf(
        "%s/%s",
        $self->storage_bucket,
        $filename,
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

aaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
