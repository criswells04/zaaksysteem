package Zaaksysteem::Zaken::Roles::Export;
use Moose::Role;

require Zaaksysteem::Attributes;
use DateTime::Format::Strptime;
use BTTW::Tools;
use feature qw(state);

=head1 METHODS

=cut

requires qw(
    get_requested_columns
    get_v0_view
    log
);

has attribute_value_types => (
    is => 'rw',
    isa => 'HashRef',
    lazy => 1,
    builder => '_build_attribute_value_type',
    traits => [qw[Hash]],
    handles => {
        has_value_type_for_magic_string => 'exists',
        get_value_type_for_magic_string => 'get',
    }
);

sub _build_attribute_value_type {
    my $self = shift;

    my $rs = $self->result_source->schema->resultset('BibliotheekKenmerken');
    my @requested_columns = $self->get_requested_columns;

    my @attributes;
    foreach my $name (@requested_columns) {
        next if $name =~ m/^case\./;
        $name =~ s/^attribute\.//g;
        push(@attributes, $name);
    }
    my %result;
    my $attributes = $rs->search_rs({ magic_string => { -in => \@attributes }});
    while (my $attr = $attributes->next) {
        $result{'attribute.' . $attr->magic_string} = $attr->value_type;
    }
    return \%result;

}

sub _build_csv_header {
    my $self        = shift;
    my @show_column = $self->get_requested_columns;

    my $rs = $self->result_source->schema->resultset('BibliotheekKenmerken');

    my @header;
    foreach (@show_column) {
        my $attr = Zaaksysteem::Attributes->get_plain_attribute_by_name($_);
        if ($attr) {
            push(@header, $attr->{label});
        }
        else {
            my $name = $_;
            $name =~ s/attribute.//g;

            my $found = $rs->search_rs({ magic_string => $name })->first;
            push(@header, $found ? $found->naam : $_);

        }
    }
    return \@header;
}

sub _format_timestamp {
    my $value = shift;
    my $strp  = DateTime::Format::Strptime->new(pattern => '%FT%T%z');

    if (my $val = $strp->parse_datetime($value)) {
        $value = $val->set_time_zone('Europe/Amsterdam')
            ->strftime('%d-%m-%Y %T');
    }
    return $value;
}

sub _format_date {
    my $value = shift;
    my $strp  = DateTime::Format::Strptime->new(pattern => '%FT%T%z');

    if (my $val = $strp->parse_datetime($value)) {
        $value = $val->set_time_zone('Europe/Amsterdam')
            ->strftime('%d-%m-%Y');
    }
    return $value;
}

state %formatters = (
    timestamp_or_text => \&_format_timestamp,
    date              => \&_format_date,
    integer           => sub { return int(shift) },
    confidentiality   => sub {
        my $value = shift;
        return $value->{mapped};
    },
    deadline => sub {
        my $value = shift;

        if (ref $value eq 'HASH') {
            $value = [$value];
        }

        if (ref $value eq 'ARRAY') {

            my $build_single_value = sub {
                my $value = shift;
                return sprintf(
                    "Fase %s: %d van de %d",
                    $value->{phase_no} // '<onbekend>',
                    $value->{current}  // 0,
                    $value->{days}     // 0
                );
            };

            # Timeline is array
            my @val = map { $build_single_value->($_) } @{$value};
            return join("; ", @val);
        }
        return '';
    },
    bag => sub {
        my $value = shift;

        if (ref $value eq 'HASH') {
            $value = [$value];
        }

        if (ref($value) eq 'ARRAY') {
            my @val
                = grep { defined $_ } map { $_->{human_identifier} } @{$value};
            return join("; ", @val);
        }
        return '';
    },

);

sub _build_csv_data {
    my $self        = shift;
    my @show_column = $self->get_requested_columns;
    my $data        = $self->get_v0_view;

    my @csv_values;

    foreach my $col (@show_column) {

        if ($self->is_blacklisted($col)) {
            push(@csv_values, "Geen rechten");
            next;
        }

        my $attr = Zaaksysteem::Attributes->get_plain_attribute_by_name($col);
        if ($attr) {
            push(@csv_values,
                $self->_format_csv_values($attr, $data->{values}{$col}));
            next;
        }

        push(@csv_values,
            $self->_format_csv_values_attributes($col, $data->{values}{$col}));
    }
    return \@csv_values;
}

sub _format_csv_values_attributes {
    my ($self, $name, $value) = @_;

    return '' unless defined $value;
    if (ref $value ne 'ARRAY') {
        $value = [ $value ];
    }
    return '' if !@$value;

    my $value_type = $self->get_value_type_for_magic_string($name);

    if ($value_type eq 'valuta') {
        $value = sprintf("%.2f", $value->[0]);
        $value =~ s/\./,/;
        return $value;
    }

    if ($value_type eq 'file' && exists $value->[0]{filename}) {
        return join(", ", map { $_->{filename} } @$value);
    }

    if ($value_type eq 'relationship' && exists $value->[0]{label}) {
        return join(", ", map { $_->{label} } @$value);
    }

    if ($value_type =~ m/^bag_/ && exists $value->[0]{address_data}) {
        return join(", ", map { $_->{human_identifier} // $_->{bag_id} } @$value);
    }

    if ($value_type eq 'date') {
        return join (", ", map { _format_date($_) } @$value);
    }

    if (ref $value->[0] eq 'HASH') {
        return join(", ", map { dump_terse($_) } @$value);
    }

    return join(", ", @$value);
}

sub _format_csv_values {
    my $self  = shift;
    my $attr  = shift;
    my $value = shift;

    return '' unless defined $value;

    my $type = $attr->{attribute_type} // 'text';

    return $value if $type eq 'text';

    if (exists $formatters{$type}) {
        return $formatters{$type}->($value);
    }
    return $value;
}

=head2 export_files

Export files for documentenlijst. Search results > Exporteren > Documentenlijst

=cut

sub export_files {
    my $self = shift;

    my @res;
    my $files = $self->active_files;
    while (my $file = $files->next) {
        if ($file->get_column('filestore_id') && !$file->filestore) {

            # virus
            next;
        }

        push(
            @res,
            [
                $file->filepath,
                $file->filestore->mimetype,
                $file->date_created->strftime('%d-%m-%Y %H:%M:%S'),
                'zaakbehandeling',
                $self->status eq 'open' ? 'Open' : 'Gearchiveerd',
                $file->document_status,
                $self->id,
                $file->version,
            ]
        );
    }
    return @res;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
