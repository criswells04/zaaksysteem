package Zaaksysteem::External::KvKAPI;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::External::KvKAPI - KvKAPI model for Zaaksysteem

=head1 DESCRIPTION

Glue layer for KvKAPI calls in Zaaksysteem. This module calls the KvK API and
allows you to transform the data from ZS syntax to KvK and transforms KvK
objects to ZS objects.

=head1 SYNOPSIS

    use Zaaksysteem::External::KvKAPI;

    my $api = Zaaksysteem::External::KvKAPI->new(
        api_key => 'foo',
    );

    $api->search;
    $api->search_all;
    $api->search_max;

    $api->api_call;

=cut

use WebService::KvKAPI;

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::AddressIncomplete;;
use Zaaksysteem::Object::Types::Company;
use Zaaksysteem::Object::Types::Company::Activity;
use Zaaksysteem::Object::Types::CountryCode;
use Zaaksysteem::Object::Types::LegalEntityType;
use Zaaksysteem::Object::Types::Subject;
use Zaaksysteem::Object::Types::ExternalSubscription;
use List::Util qw(first);

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 api_key

The API key of the KvK API

=cut

has api_key => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 spoofmode

Boolean value indicating that the calls should go against the test api
server at KvK.

=cut

has spoofmode => (
    is       => 'ro',
    isa      => 'Bool',
    default  => 0,
);

=head2 api_host

String value indicating the host to be used explicitly, instead of relying
on the host specified in the OpenAPI specification.

=cut

has api_host => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_api_host'
);

=head2 api

The API object, should be a L<WebService::KvKAPI> kinda object. You can only
set this via the constructor.

It delegates the following actions: C<search>, C<search_all>, C<search_max> and
C<profile>, so you can call C<< $self->search> >>. For more information about
the methods see L<WebService::KvKAPI>.

=cut

has _api => (
    is      => 'ro',
    isa     => 'WebService::KvKAPI',
    builder => '_build_kvk_api',
    lazy    => 1,
    handles => [qw(
        get_location_profile
        get_basic_profile
        get_owner
        get_main_location
        get_locations
    )],
    init_arg => 'api',
);

sub search {
    my $self = shift;
    my $rv = $self->_api->search(@_);
    return $rv->{resultaten} if $rv;
    return;
}

=head2 query_map

Map L<Zaaksysteem::Object::Types::Subject> attribute names to KvK API query
parameters. You can override the default values via the constructor.

Defaults to:

    {
        coc_number                          => 'kvkNumber',
        coc_location_number                 => 'branchNumber',
        company                             => 'tradeName',
        rsin                                => 'rsin',

        'address_residence.street'          => 'street',
        'address_residence.street_number'   => 'houseNumber',
        'address_residence.zipcode'         => 'postalCode',
        'address_residence.city'            => 'city',
    }

=cut

has _query_map => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        return {
            coc_number                          => 'kvkNummer',
            coc_location_number                 => 'vestigingsnummer',
            company                             => 'handelsnaam',
            rsin                                => 'rsin',
            oin                                 => 'oin',

            'address_residence.street'          => 'straat',
            'address_residence.street_number'   => 'huisnummer',
            'address_residence.zipcode'         => 'postcode',
            'address_residence.city'            => 'plaats',
        };
    },
    traits => ['Hash'],
    handles => {
        exists_query_key => 'exists',
        get_query_key    => 'get',
    },
    init_arg => 'query_map',
);

=head1 METHODS

Implements (almost) all methods defined by L<WebService::KvKAPI>.

=head2 exists_query_key

Check if a query key can be mapped to a KvK key

=head2 get_query_key

Get the KvK query key based on the ZS key

=cut

sub _build_kvk_api {
    my $self = shift;

    my %kvk_args = (
        api_key        => $self->api_key,
    );

    if ($self->spoofmode) {
        $kvk_args{spoof} = 1;
    }

    if ($self->has_api_host) {
        $kvk_args{ api_host } = $self->api_host;
    }

    return WebService::KvKAPI->new(%kvk_args);
}

=head2 map_params_to_kvk_query

Map the ZS internal Zaaksysteem subject fields to a KvK query

=cut

sig map_params_to_kvk_query => 'HashRef';

sub map_params_to_kvk_query {
    my ($self, $params) = @_;

    my %kvk_query;
    foreach (keys %$params) {
        next unless $self->exists_query_key($_);
        $kvk_query{$self->get_query_key($_)} = $params->{$_};
    }


    return \%kvk_query if keys %kvk_query;

    throw('kvk/params/mapping', 'No KvK query can be build!');
}

=head2 map_kvk_company_to_subject

Map the output of the KvK API to a customer

=cut

sig map_kvk_company_to_subject =>
    'HashRef => Zaaksysteem::Object::Types::Subject';

sub _build_sbi_object {
    my $data = shift;
    return Zaaksysteem::Object::Types::Company::Activity->new(
        code        => $data->{sbiCode},
        description => $data->{sbiOmschrijving},
    );
}

sub _get_legal_entity_type {
    my ($self, $company) = @_;
    my $embed = $company->{_embedded};
    return unless $embed;
    return try {
        Zaaksysteem::Object::Types::LegalEntityType->new_from_label(
            $embed->{eigenaar}{uitgebreideRechtsvorm}
                // $company->{eigenaar}{rechtsvorm} // '')
    }
    catch {
        $self->log->info($_);
        return;
    };
}

sub map_kvk_company_to_subject {
    my ($self, $company) = @_;

    my $address_residence = _map_kvk_address_to_subject_address(
        _get_kvk_address_residence($company)
    );

    my $address_correspondence = _map_kvk_address_to_subject_address(
        _get_kvk_address_correspondence($company)
    );

    my $main_sbi;
    my @other_sbi;
    if ($company->{sbiActiviteiten}) {
        my @activities = @{$company->{sbiActiviteiten}};
        my $main = first { $_->{indHoofdactiviteit} eq 'Ja' } @activities;
        my @rest = grep { $_->{indHoofdactiviteit} eq 'Nee' } @activities;

        $main_sbi = _build_sbi_object($main);

        foreach (@rest) {
            push(@other_sbi, _build_sbi_object($_));
        }
    }

    my $company_type = $self->_get_legal_entity_type($company);

    my $subject = Zaaksysteem::Object::Types::Company->new(
        company => $company->{handelsnaam} // $company->{naam}
            // $company->{eersteHandelsnaam},

        coc_number => $company->{kvkNummer},

        $company->{vestigingsnummer} ?
            (coc_location_number => $company->{vestigingsnummer})
            : (),

        $company->{materieleRegistratie}{datumAanvang} ? (
            date_registration => _to_dt($company->{materieleRegistratie}{datumAanvang})
        ) : (),

        $company->{formeleRegistratiedatum} ? (
            date_founded => _to_dt($company->{formeleRegistratiedatum})
        ) : (),

        $company->{materieleRegistratie}{datumEinde} ? (
            date_ceased => _to_dt($company->{materieleRegistratie}{datumEinde})
        ) : (),

        $company->{rsin}   ? (rsin => $company->{rsin}) : (),
        $company->{oin}    ? (oin => $company->{oin}) : (),

        $company_type      ? (company_type => $company_type)
            : (),

        $address_residence ? (address_residence => $address_residence)
            : (),

        $address_correspondence ?
            (address_correspondence => $address_correspondence)
            : (),

        $main_sbi ? (main_activity => $main_sbi) : (),
        @other_sbi ? (secondairy_activities => \@other_sbi) : (),
    );

    return Zaaksysteem::Object::Types::Subject->new(
        subject_type => 'company',
        subject      => $subject,

        external_subscription =>
            Zaaksysteem::Object::Types::ExternalSubscription->new(
                external_identifier => _generate_unique_identifier($company)
            )
    );

}

sub _to_dt {
    my $date = shift;

    my $p = DateTime::Format::Strptime->new(
        pattern   => '%Y%m%d',
        locale    => 'nl_NL',
        time_zone => "floating",
    );

    # Force month/day concept on dates
    $date =~ s/0000$/0101/;
    $date =~ s/00$/01/;

    # Force dutchy time
    return $p->parse_datetime($date)->add(hours => 12);
}

sub _generate_unique_identifier {
    my $company = shift;

    return join( '/' =>
        $company->{kvkNummer},
        $company->{vestigingsnummer} // '',
        $company->{rsin} // '',
    );
}

sub _get_kvk_address_residence {
    return _get_kvk_address_by_type($_[0], 'vestigingsadres', 'bezoekadres');
}

sub _get_kvk_address_correspondence {
    return _get_kvk_address_by_type($_[0], 'correspondentieadres');
}

sub _get_kvk_address_by_type {
    my ($company, @types) = @_;

    my $addresses;
    # Basisprofiel
    if (exists $company->{_embedded}) {
        if (exists $company->{_embedded}{hoofdvestiging}) {
            $addresses = $company->{_embedded}{hoofdvestiging}{adressen};
        }
        else {
            $addresses = $company->{_embedded}{eigenaar}{adressen};
        }
    }
    # locatieprofiel
    elsif (exists $company->{adressen}) {
        $addresses = $company->{adressen};
    }
    # search
    elsif (exists $company->{straatnaam}) {
        return $company;
    }
    else {
        return;
    }

    foreach my $type (@types) {
        foreach my $address (@{$addresses}) {
            return $address if $address->{type} eq $type;
        }
    }
    return;
}

sub _map_kvk_address_from_search {
    my ($kvk_address) = @_;

    if (!$kvk_address->{straatHuisnummer}) {
        return Zaaksysteem::Object::Types::AddressIncomplete->new(
            street => $kvk_address->{straatnaam},
            city   => $kvk_address->{plaats},
        );
    }
    else {
        return Zaaksysteem::Object::Types::AddressIncomplete->new(
            foreign_address_line1 => $kvk_address->{straatHuisnummer},
        );
    }
}

sub _map_kvk_address_full {
    my ($kvk_address) = @_;

    my $country = Zaaksysteem::Object::Types::CountryCode->new_from_name(
        $kvk_address->{land});

    if ($country->dutch_code eq '6030') {

        my $lat    = $kvk_address->{geoData}{gpsLatitude};
        my $long   = $kvk_address->{geoData}{gpsLongitude};
        my $bag_id = $kvk_address->{geoData}{nummerAanduidingId};

        return Zaaksysteem::Object::Types::Address->new(
            $kvk_address->{postbusnummer} ?
            (
                street => 'Postbus',
                street_number => $kvk_address->{postbusnummer},
            ) : (
                street        => $kvk_address->{straatnaam},
                street_number => $kvk_address->{huisnummer},
                $kvk_address->{huisnummerToevoeging} ? (
                    street_number_suffix => $kvk_address->{huisnummerToevoeging},
                ): (),
                $kvk_address->{huisletter} ? (
                    street_number_letter => $kvk_address->{huisletter},
                ) : (),
            ),

            city    => $kvk_address->{plaats},
            zipcode => $kvk_address->{postcode},

            $bag_id ? (bag_id => $bag_id) : (),

            $lat && $long
            ? (latitude => $lat, longitude => $long)
            : (),

            country => $country,
        );
    }
    else {
        # TODO: Look on the CPAN for address mangling modules
        # for now accept dutchy style notations
        return Zaaksysteem::Object::Types::Address->new(
            foreign_address_line1 => $kvk_address->{straatHuisnummer},
            foreign_address_line2 => $kvk_address->{postcodeWoonplaats},
            country => $country,
        );
    }
}


sub _map_kvk_address_to_subject_address {
    my $kvk_address = shift;

    return unless $kvk_address;

    return $kvk_address->{land}
        ? _map_kvk_address_full($kvk_address)
        : _map_kvk_address_from_search($kvk_address);

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a
look at the L<LICENSE|Zaaksysteem::LICENSE> file.
