package Zaaksysteem::Search::Case::ZQL;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Search::Case::ZQL - ZQL for Case

=head1 DESCRIPTION

A migration class for Zaaksysteem::Search::ZQL to remove the dependency to object data

=head1 SYNOPSIS

    use Foo;

=cut

extends 'Zaaksysteem::Search::ZQL';

after BUILD => sub {
    my $self = shift;

    return if $self->object_type eq 'case';
    throw("case/zql/object_type", "Invalid object type in ZQL query, not a case");
};

sub _grammar_syntax_class { 'Zaaksysteem::Search::Case::ZQL::Syntax' }

sub _prepare_resultset {
    my ($self, $rs) = @_;
    return $rs;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
