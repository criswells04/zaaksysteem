package Zaaksysteem::Roles::ZQL;
use Moose::Role;

with qw(
    Zaaksysteem::Search::HStoreResultSet
    Zaaksysteem::Search::TSVectorResultSet
    Zaaksysteem::Search::ZQL::Role::ResultSet
);

sub hstore_column { 'index_hstore' };
sub text_vector_column { 'text_vector' };

sub apply_zql_roles {
    my $self = shift;

    foreach my $row (@_) {
        next unless blessed($row);
        $row->_initialize_zql($self->{attrs}{_zql_options} // {});
    }
    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
