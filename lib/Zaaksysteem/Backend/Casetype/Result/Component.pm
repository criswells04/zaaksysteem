package Zaaksysteem::Backend::Casetype::Result::Component;
use Moose;

extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::Backend::Case::Result - A Zaaksysteem::Backend::Case::Result package

=cut

sub TO_JSON {
    my $self = shift;

    return {
        id                 => $self->id,
        label              => $self->label,
        generic_type       => $self->resultaat,
        external_reference => $self->external_reference,
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
