package Zaaksysteem::Backend::Sysin::Modules::ControlPanel;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::ControlPanel - ControlPanel interface

=head1 SYNOPISIS

=cut

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Types qw(UUID);

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    /;

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(
        name             => 'controlpanel',
        label            => 'Control Panel',
        interface_config => [
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_domain',
                type        => 'text',
                label       => 'Domeinnaam',
                required    => 1,
                description => 'De domeinnaam waaronder alle controlpanel omgevingen onder aangemaakt gaan worden',
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_api_key',
                type        => 'text',
                label       => 'API key',
                required    => 0,
                description => 'Interface key voor controlpanel',
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_pip',
                type        => 'checkbox',
                label       => 'PIP visibility',
                required    => 0,
                description => 'Maak de koppeling zichtbaar op de PIP',
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'interface_pip_write',
                type        => 'checkbox',
                label       => 'PIP write',
                required    => 0,
                description => 'Sta doet dat gebruikers op de PIP controlpanel objecten kunnen wijzigen. Dit is handig tijdens migraties',
            ),
            Zaaksysteem::ZAPI::Form::Field->new(
                name     => 'interface_medewerker',
                type     => 'spot-enlighter',
                label    => 'Medewerker',
                required => 0,
                description =>
                    'Bepaal hier welke medewerkerrechten de externe partij krijgt.',
                data => {
                    restrict    => 'contact/medewerker',
                    placeholder => 'Type uw zoekterm',
                    label       => 'naam',
                }
            ),
        ],
        direction                     => 'incoming',
        manual_type                   => ['text'],
        is_multiple                   => 0,
        is_manual                     => 0,
        retry_on_error                => 0,
        allow_multiple_configurations => 0,
        is_casetype_interface         => 0,
        has_attributes                => 0,
        trigger_definition            => { },
        module_type                   => ['apiv1','controlpanel'],
        test_interface => 0,
        sensitive_config_fields => ['api_key'],

    );
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
