package Zaaksysteem::Backend::Rules::Rule::Condition;

use Moose;
use Moose::Util::TypeConstraints;
use List::Util qw(any);
use BTTW::Tools;


use Zaaksysteem::Types qw/Boolean/;

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Rules::Rule::Condition::Confidentiality
    Zaaksysteem::Backend::Rules::Rule::Condition::PresetClient
    Zaaksysteem::Backend::Rules::Rule::Condition::Properties
    Zaaksysteem::Backend::Rules::Rule::Condition::PaymentStatus
    Zaaksysteem::Backend::Rules::Rule::Condition::Contactchannel
    Zaaksysteem::Backend::Rules::Rule::Condition::PreferredContactchannel
    Zaaksysteem::Backend::Rules::Rule::Condition::Zipcode
    Zaaksysteem::Backend::Rules::Rule::Condition::Area
    Zaaksysteem::Backend::Rules::Rule::Condition::RequestorType
/;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition - A single condition

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 attribute

isa: String

The attribute name this condition is base on, e.g. "attribute.beermanufacturer"

=cut

has 'attribute'    => (
    is      => 'rw',
    isa     => 'Str',
);

=head2 _schema

DBIx::Class::Schema instance

=cut

has '_schema'    => (
    is      => 'rw',
    isa     => 'DBIx::Class::Schema',
);

=head2 attribute_map

Attribute mapping

=cut

has 'attribute_map'    => (
    is      => 'rw',
    isa     => 'HashRef',
);

=head2 value

isa: ArrayRef

The values the given C<attribute> must match.

=cut

has 'values'    => (
    is      => 'rw',
    isa     => 'ArrayRef',
);

=head2 rules_params

isa: HashRef

Given source_params in L<Zaaksysteem::Backend::Rules>

=cut

has 'rules_params'      => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);


=head2 validation_type

isa:

=cut

has 'validation_type' => (
    is          => 'rw',
    isa         => subtype('Str' => where { $_ =~ m/immediate|fixed|revalidate/ }),
    required    => 1,
    default     => 'immediate',
);


=head2 validates_true

isa: Bool

When set to true, this condition has met his conditions already, e.g.: backend
already decided this is a true value. Think of a match on postalcode or
contactchannel.

=cut

has 'validates_true' => (
    is          => 'rw',
    isa         => Boolean,
    predicate   => 'has_validates_true',
    clearer     => 'clear_validates_true',
);

has '_old_kenmerk_id' => (
    is  => 'rw',
);

=head2 validate

Matches the given parameters against this condition. If it matches, return 1

=cut

sub validate {
    my $self    = shift;
    my $params  = shift;

    return 1 if $self->validates_true;

    my @values_in_rule = grep { defined $_ && length($_) } @{ $self->values };

    if (!$params->{ $self->attribute } && !@values_in_rule) {
        # Auto matching rule because the value does not exist in the case
        return 1;
    }

    my @given_values = ref $params->{ $self->attribute } eq 'ARRAY'
        ? @{ $params->{ $self->attribute } }
        : ($params->{ $self->attribute });

    @given_values = grep { defined $_ && length($_) } @given_values;

    for my $value (@values_in_rule) {
        if (any { $_ eq $value } @given_values) {
            return 1;
        }
    }

    return;
}

sub BUILD {}

sub TO_JSON {
    my $self        = shift;

    return {
        attribute_name  => $self->attribute,
        values          => $self->values,
        validation_type => $self->validation_type,
        $self->has_validates_true ? (validates_true => $self->validates_true) : ()
    };
}

1;

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

