# Testing

> ECMAScript unit testing with [Jest](https://facebook.github.io/jest/).

Jest comes with [jsdom](https://github.com/tmpvar/jsdom).
This enables you to run tests that depend on browser APIs 
without using a browser and a web server. 

NB: jsdom is a conforming DOM implementation for Node.js, 
not a library of test fakes/mocks/stubs.
If you need DOM APIs that are not implemented (e.g. local storage, 
media queries), you need to install or write them yourself
(cf. `/client/test/jest-setup.js`).

## Configuration

See `/client/jest.config.js` and the 
[configuration documentation](https://facebook.github.io/jest/docs/en/configuration.html).

## Running tests

    $ npm test

### Only test uncommitted files

    $ npm test -- -o

### Show nested suites and all tests in `stdout`

    $ npm test -- --verbose

### TDD

    $ npm test -- --watch

or if you want to run all tests when a file has changeded

    $ npm test -- --watchAll

### Coverage

    $ npm test -- --coverage

### CLI options documentation

https://facebook.github.io/jest/docs/en/cli.html

## Writing tests

Create the test modules in the file system next to 
the module under test and append `.test` to the 
file's base name, e.g.

- `/client/src/shared/fooBar.js`
- `/client/src/shared/fooBar.test.js`

### “Proper” unit tests

Simply `import` the module under test. Jest provides 
[global identifiers](https://facebook.github.io/jest/docs/en/api.html),
the most important ones being `test` and `expect`.

    import moduleUnderTest from './moduleUnderTest';
    
    test('moduleUnderTest', () => {
      expect(moduleUnderTest.name).toBe('silly');
    });

The `expect` function provides Jasmine-like 
[matchers](https://facebook.github.io/jest/docs/en/expect.html)
and you can group test in suites if you wish:

    import { foo, bar } from './moduleUnderTest';

    describe('test suite', () => {
      test('foo', () => {
        expect(foo).toBe('function');
      });

      test('bar', () => {
        expect(bar).toBe('function');
      });
    });
    
As a matter of fact, there's a lot of the Jasmine API around to 
make migration easy, e.g. a global `jasmine` object and 
`it` as an alias for `test`.

### AngularJS tests

Only use `angular` and its mocking monkey patches if you 
cannot extract the code under test, or if you need to write 
tests before doing so. Do not make angular global in the 
setup file, make dependencies explicit:

    import angular from 'angular';
    import 'angular-mocks';
    import moduleUnderTest from './moduleUnderTest';

Then you need some ceremony for the dependency injection;
since tests are executed in the Node.js runtime environment,
never use the `module` global that is exposed by `angular-mocks`,
use `angular.mock.module`:

    describe('moduleUnderTest', () => {
      let scope;
      let element;
      let controller;

      beforeEach(angular.mock.module(moduleUnderTest));

      beforeEach(angular.mock.inject([
        '$rootScope',
        ( ...rest ) => {
          // Assign any values that need DI to variables in the parent scope.
          [$rootScope] = rest;
          
          // typical boilerplate for testing directives
          scope = $rootScope.$new();
          element = angular.element('<module-under-test></module-under-test>');
          $compile(element)(scope);
          controller = element.controller('moduleUnderTest');
        }
      ]));
      
      test('...' => {
        // ...
      })
    );

AngularJS unit testing is a very complex 
topic and beyond the scope of this document. 
See the 
[documentation](https://docs.angularjs.org/guide/unit-testing) 
for more information. 
