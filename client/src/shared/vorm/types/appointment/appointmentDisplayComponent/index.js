// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import controller from './AppointmentDisplayController';
import template from './template.html';

controller.$inject = [
  '$scope',
  '$http',
  'resource',
  'composedReducer',
  'dateFilter',
  'snackbarService',
];

export default {
  bindings: {
    value: '&',
    onRemove: '&',
    provider: '&',
  },
  controller,
  template,
};
