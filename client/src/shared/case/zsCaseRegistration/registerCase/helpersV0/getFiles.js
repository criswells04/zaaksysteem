// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import map from 'lodash/map';
import find from 'lodash/find';
import assign from 'lodash/assign';

const getFiles = (files, documents) =>
  map(files, ({ reference, name, origin, origin_date, description }) => {
    const original = find(documents, (d) => d.filestore_id.uuid === reference);

    return {
      reference,
      name,
      metadata: assign({}, original.metadata_id, {
        origin,
        origin_date: origin_date ? `${origin_date}` : null,
        description,
      }),
    };
  });

export default getFiles;
