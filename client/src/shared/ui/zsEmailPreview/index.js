// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import seamlessImmutable from 'seamless-immutable';

export default angular
  .module('shared.ui.zsEmailPreview', [])
  .directive('zsEmailPreview', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          previewData: '=',
          caseId: '&',
          emailTemplateData: '&',
        },
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let iframeLoaded = false;
            let queuedPreviewerUpdate;
            let previewData = seamlessImmutable({});

            let iframe = document.createElement('iframe');

            iframe.id = 'iframe-email-template';
            iframe.title = 'iframe-email-template';
            iframe.alt = 'email-template-body';
            iframe.classList.add('iframe-email-template');
            iframe.src = '/main/email-template-preview';
            iframe.style.width = '100%';
            iframe.style.border = 'none';
            $element.find('body-preview').replaceWith(iframe);

            function resizeIframe(ev) {
              if (ev.data.name === 'resizeEmailPreviewer') {
                let iframe = document.getElementById('iframe-email-template');
                const iframeHeight =
                  200 +
                  iframe.contentWindow.document.documentElement
                    .querySelector('#shadow')
                    .getBoundingClientRect().height;

                // eslint-disable-next-line prefer-template
                iframe.style.height = iframeHeight + 40 + 'px';
              }
            }

            function updateIframeLoaded(ev) {
              if (ev.data.name === 'emailPreviewerLoaded') {
                iframeLoaded = true;
                queuedPreviewerUpdate && queuedPreviewerUpdate();
              }
            }

            $scope.$watch('previewData', (values, oldValues) => {
              if (values && !angular.equals(values, oldValues)) {
                const updatePreviewer = () =>
                  iframe.contentWindow.postMessage(
                    {
                      name: 'previewReload',
                      value: Object.fromEntries(
                        [
                          Object.entries($scope.emailTemplateData() || {}),
                          Object.entries(values),
                          [['caseId', $scope.caseId()]],
                        ].flat()
                      ),
                    },
                    '*'
                  );

                iframeLoaded
                  ? updatePreviewer()
                  : (queuedPreviewerUpdate = updatePreviewer);
              }
            });

            window.top.addEventListener('message', resizeIframe);
            window.top.addEventListener('message', updateIframeLoaded);

            $scope.$on('destroy', () => {
              window.top.removeEventListener('message', resizeIframe);
              window.top.removeEventListener('message', updateIframeLoaded);
            });

            $scope.useHtmlEmailTemplate = () =>
              Boolean($scope.emailTemplateData());

            $scope.getPreviewData = () => previewData;
          },
        ],
      };
    },
  ]).name;
