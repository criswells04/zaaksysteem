// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular
  .module('Zaaksysteem.intern.zsBackdropService', [])
  .factory('zsBackdropService', [
    () => {
      let isActive = false;

      return {
        isEnabled: () => isActive,
        setActive: () => {
          isActive = true;
          return true;
        },
        setDisabled: () => {
          isActive = false;
          return true;
        },
      };
    },
  ]).name;
