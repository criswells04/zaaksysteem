// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsCaseRegistrationModule from './../../../shared/case/zsCaseRegistration';

export default angular.module('Zaaksysteem.intern.register', [
  zsCaseRegistrationModule,
]).name;
