// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import find from 'lodash/find';
import shortId from 'shortid';
import { searchHashExpression } from '../../../../shared/util/searchHash';
import triggerDownload from '../../../../shared/util/dom/triggerDownload';

export default class ExternalDocumentViewController {
  static get $inject() {
    return [
      '$rootScope',
      '$scope',
      '$document',
      '$window',
      '$state',
      '$http',
      'composedReducer',
      'externalSearchService',
      'snackbarService',
    ];
  }

  constructor(
    $rootScope,
    scope,
    $document,
    $window,
    $state,
    $http,
    composedReducer,
    externalSearchService,
    snackbarService
  ) {
    const ctrl = this;

    ctrl.searchQuery = '';

    const fieldReducer = composedReducer({ scope }, ctrl.document()).reduce(
      (doc) =>
        doc.map((field) => ({
          id: shortId(),
          name: field.name,
          label: field.label,
          value: get(field, 'value') || '-',
        }))
    );

    const relatedDocumentReducer = composedReducer(
      { scope },
      ctrl.relatedDocuments
    ).reduce((documents) =>
      documents.map((doc) => ({
        id: shortId(),
        label: get(doc, '_source.Filename'),
        link: $state.href('externaldocument', {
          next2knowSearchIndex: get(doc, '_index'),
          next2knowdocumentId: get(doc, '_id'),
        }),
      }))
    );

    ctrl.getFields = fieldReducer.data;

    ctrl.getImage = () => ctrl.thumbnail();

    ctrl.getFilename = () =>
      get(
        find(
          fieldReducer.data(),
          (field) => field.name === 'Filename' && field.value !== '-'
        ),
        'value'
      );

    ctrl.isDownloadable = () => Boolean(ctrl.getFilename());

    ctrl.downloadDocument = () => {
      const filename = ctrl.getFilename();

      snackbarService.wait('Het bestand wordt gedownload', {
        promise: $http(
          externalSearchService.getDownloadRequestOptions(filename)
        ).then((response) => {
          triggerDownload(response, filename);
        }),
        then: () => '',
        catch: () =>
          'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.',
      });
    };

    ctrl.getRelatedDocuments = relatedDocumentReducer.data;

    ctrl.openSpotenlighter = () => {
      $rootScope.$emit('spotenlighter:open', this.searchQuery);
    };
  }

  setSearchQuery() {
    const { hash } = window.location;
    const match = searchHashExpression.exec(hash);

    if (match) {
      this.searchQuery = decodeURIComponent(match[1]);
      return true;
    }

    return false;
  }
}
