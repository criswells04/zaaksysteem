#!/usr/bin/env perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::DashboardRemoval;

my $cli = Zaaksysteem::CLI::DashboardRemoval->init();

# Run the script.
$cli->run();

1;

__END__

=head1 NAME

dashboard-removal.pl - A dashboard removal script

=head1 DESCRIPTION

Cleans the dashboard of a user, yay!

=head1 OPTIONS

=over

=item username

The username of the user

=item id

The subject ID of the user

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
