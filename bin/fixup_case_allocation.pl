#!/usr/bin/env perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use autodie;
use DateTime;
use IO::Handle;
use Log::Log4perl qw(:easy);
use Text::CSV_XS;
use Try::Tiny;
use Zaaksysteem::CLI;
use Zaaksysteem::Zaken::DelayedTouch;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my %opt = %{ $cli->options };

die "Missing option: 'zql' (-o zql='SELECT ...') \n" if (not exists $opt{zql});
die "Missing option: 'group' (-o group='1234') \n"   if (not exists $opt{group});
die "Missing option: 'role' (-o role='1234') \n"     if (not exists $opt{role});

if (not exists $opt{auditlog}) {
    $opt{auditlog} = "afdeling_bulk_log-" . DateTime->now->ymd . ".csv";
}

INFO("Logging CSV to $opt{auditlog}\n");
open my $auditlog,  ">", $opt{auditlog}; $auditlog->autoflush(1);

my $csv = Text::CSV_XS->new({ binary => 1, eol => "\015\012" });
$csv->print(
    $auditlog,
    [
        "timestamp",
        "zaaknummer",
        "afdeling_oud_id",
        "afdeling_oud_naam",
        "rol_oud_id",
        "rol_oud_naam",
        "afdeling_nieuw_id",
        "afdeling_nieuw_naam",
        "rol_nieuw_id",
        "rol_nieuw_naam",
    ]
);

my $delayed = Zaaksysteem::Zaken::DelayedTouch->new;
$cli->schema->default_resultset_attributes->{delayed_touch} = $delayed;

$cli->do_transaction(sub {
    my $self = shift;
    my $schema = shift;

    my $group = $schema->resultset('Groups')->find($opt{group});
    die "Group '$opt{group}' not found\n" if not $group;
    my $role = $schema->resultset('Roles')->find($opt{role});
    die "Role '$opt{role}' not found\n" if not $role;

    INFO("Assigning cases to group '" . $group->name . "', role '" . $role->name ."'");

    my $model = Zaaksysteem::Object::Model->new(schema => $schema);

    my $case_iterator = $model->zql_search($opt{zql});

    # Ideally, we'd iterate over objects and set values there, but case is
    # still "special".
    my $object_rs = $case_iterator->rs->search(undef, {columns => 'me.object_id'});
    my $case_rs = $schema->resultset('Zaak')->search_extended(
        { 'me.id' => { '-in' => $object_rs->as_query } },
        { order => 'me.id' }
    );

    while (my $case = $case_rs->next) {
        INFO("Updating case " . $case->get_column('id'));

        my $old_group_id = $case->route_ou;
        my $old_group    = $schema->resultset('Groups')->find($old_group_id);

        my $old_role_id  = $case->route_role;
        my $old_role     = $schema->resultset('Roles')->find($old_role_id);

        $case->wijzig_route(
            {
                route_ou                 => $opt{group},
                route_role               => $opt{role},
                change_only_route_fields => 1,
            }
        );

        $csv->print(
            $auditlog,
            [
                DateTime->now()->iso8601,
                $case->get_column('id'),
                $old_group_id,
                $old_group ? $old_group->name : 'unknown-group',
                $old_role_id,
                $old_role ? $old_role->name : 'unknown-role',
                $opt{group},
                $group->name,
                $opt{role},
                $role->name,
            ]
        );
    }

    # Will die (and roll back transaction) on disk-full/other disk exceptions.
    close $auditlog;
});

INFO("All done. Don't forget to run touch-case.pl!");

1;

__END__

=head1 NAME

fixup_case_allocation.pl - Change allocation (route_ou, route_role) on cases

=head1 SYNOPSIS

touch_case.pl OPTIONS -o zql=SELECT... -o group=ou_id -o role=role_id [ -o auditlog=logfile_name ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over

=item * zql

Required option. This ZQL is used to determine which cases to change.

=item * group

Required option. The ID of the group to set on the cases.)

=item * role

Required option. The ID of the role to set on the cases.)

=item * auditlog

Optional. The audit log CSV to write.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
