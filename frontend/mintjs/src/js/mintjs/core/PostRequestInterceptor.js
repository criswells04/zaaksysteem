/*global angular,_*/
(function ( ) {
	
	angular.module('MintJS.core')
		.config([ '$httpProvider', function ( $httpProvider ) {
			
			$httpProvider.interceptors.push([ 'windowUnloadService', function ( windowUnloadService ) {
				return {};
				
				// TODO: need a way to remove the interceptor if the request fails
				
				// var posts = [];
				
				// windowUnloadService.register(function ( ) {
				// 	if(posts.length) {
				// 		return windowUnloadService.UNSAVED_CHANGES;
				// 	}
				// 	return undefined;
				// });
				// return {
				// 	request: function ( config ) {
						
				// 		if(config.method.toLowerCase() === 'post') {
				// 			var transformer = function ( ) {
				// 				_.pull(posts, config);
				// 				_.pull(config.transformResponse, transformer);
				// 			};
				// 			config.transformResponse.push(transformer);
				// 		}
						
				// 		return config;
						
				// 	}
				// };
				
				
			}]);
		}]);
	
})();