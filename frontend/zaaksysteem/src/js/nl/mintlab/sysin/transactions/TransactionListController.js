// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin.transactions')
    .controller('nl.mintlab.sysin.transactions.TransactionListController', [
      '$scope',
      'smartHttp',
      'translationService',
      '$location',
      'formService',
      function ($scope, smartHttp, translationService, $location, formService) {
        $scope.interfaces = [];
        $scope.modules = [];
        $scope.activeTransaction = null;

        function setInterfaceId() {
          var interfaceId =
              $scope.interfaceId !== undefined ? $scope.interfaceId : '',
            form;

          form = formService.get('crud-filters-transactions');
          if (form) {
            form.setValue('interface_id', interfaceId);
          }
        }

        function setIsError() {
          var isError = $scope.isError !== undefined ? $scope.isError : false,
            form;

          isError = isError ? '1' : undefined;

          form = formService.get('crud-filters-transactions');
          if (form) {
            form.setValue('records$dot$is_error', isError);
          }
        }

        function getModuleByName(name) {
          return _.filter($scope.modules, function (module) {
            return module.name === name;
          })[0];
        }

        $scope.getInterfaceOptions = function () {
          var options = [],
            interfaces = $scope.interfaces || [],
            item,
            i,
            l,
            allId = translationService.get('Alle koppelingen');

          options.push({
            name: allId,
            value: '',
            label: allId,
          });

          for (i = 0, l = interfaces.length; i < l; ++i) {
            item = interfaces[i];
            options.push({
              name: item.name,
              value: item.id,
              label: item.name,
            });
          }

          return options;
        };

        $scope.showTransaction = function (transaction) {
          $scope.activeTransaction = transaction;
        };

        $scope.getPreviewItems = function (transaction) {
          var previewItems = transaction.result_preview;
          return previewItems;
        };

        $scope.getModuleByName = function (name) {
          return getModuleByName(name);
        };

        $scope.$watch('interfaceId', function () {
          setInterfaceId();
        });

        $scope.$watch('isError', function () {
          setIsError();
        });

        $scope.$on('form.prepare', function (event) {
          if (event.targetScope.getFormName() === 'crud-filters-transactions') {
            setInterfaceId();
            setIsError();
          }
        });

        $scope.$on('form.submit.success', function (event) {
          if (event.targetScope.getFormName() === 'manual-process') {
            $scope.$broadcast('manualprocess');
          }
        });

        smartHttp
          .connect({
            method: 'GET',
            url: '/sysin/interface',
            params: {
              zapi_no_pager: 1,
            },
          })
          .success(function (data) {
            $scope.interfaces = data.result;
          })
          .error(function () {
            $scope.$emit('systemMessage', {
              type: 'error',
              content: translationService.get(
                'Koppelingen konden niet worden opgehaald'
              ),
            });
          });

        smartHttp
          .connect({
            method: 'GET',
            url: '/sysin/interface/modules/all',
            params: {
              zapi_no_pager: 1,
            },
          })
          .success(function (data) {
            $scope.modules = data.result;
          })
          .error(function () {
            $scope.$emit('systemMessage', {
              type: 'error',
              content: translationService.get(
                'Modules konden niet worden opgehaald'
              ),
            });
          });
      },
    ]);
})();
