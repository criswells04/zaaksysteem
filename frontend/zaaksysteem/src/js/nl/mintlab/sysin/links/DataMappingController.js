// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin.links')
    .controller('nl.mintlab.sysin.links.DataMappingController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var indexOf = _.indexOf;

        $scope.mappings = [];

        function loadData() {
          smartHttp
            .connect({
              method: 'GET',
              url: '/sysin/interface/' + $scope.activeLink.id + '/mapping',
            })
            .success(function (data) {
              var config = data.result[0],
                i,
                l,
                mappings = config.attributes,
                mapping;

              for (i = 0, l = mappings.length; i < l; ++i) {
                mapping = mappings[i];
                mapping.checked = !!mapping.checked;
                mapping.optional = !!mapping.optional;
              }
              $scope.attributeType = config.attribute_type;
              $scope.mappings = mappings;
            })
            .error(function (data) {
              var error = data.result[0],
                content;

              if (error.type === 'sysin/modules/attributes/no_case_type_id') {
                content = translationService.get(
                  'Zaaktype is niet ingevuld en opgeslagen'
                );
              } else {
                content = translationService.get(
                  'Er ging iets fout bij het ophalen van de gegevensmapping'
                );
              }

              $scope.$emit('systemMessage', {
                type: 'error',
                content: content,
              });
            });
        }

        $scope.addMapping = function (name) {
          $scope.mappings.push({
            external_name: name,
            optional: true,
            checked: true,
            attribute_type: 'freeform',
          });
        };

        $scope.removeMapping = function (mapping) {
          var index = indexOf($scope.mappings, mapping);
          if (index !== -1) {
            $scope.mappings.splice(index, 1);
          }
        };

        $scope.save = function () {
          smartHttp
            .connect({
              method: 'POST',
              url:
                '/sysin/interface/' + $scope.activeLink.id + '/mapping/update',
              data: {
                attributes: $scope.mappings,
              },
            })
            .success(function () {
              $scope.close();
            })
            .error(function () {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er ging iets fout bij het opslaan van de wijzigingen'
                ),
              });
            });
        };

        $scope.getSpotEnlighterRestrict = function (mapping) {
          var restrict = '';

          if (
            mapping.attribute_type === 'magic_string' &&
            !mapping.objecttype_attribute
          ) {
            if (mapping.all_casetypes) {
              restrict = 'casetype/all/magicstring';
            } else {
              restrict = mapping.case_type_id
                ? 'casetype/' + mapping.case_type_id + '/magicstring'
                : 'casetype';
            }

            if (mapping.include_system) {
              restrict += '?include_system=1';
            }

            return restrict;
          }

          if (
            mapping.attribute_type === 'magic_string' &&
            mapping.objecttype_attribute
          ) {
            if (mapping.objecttype_id) {
              return 'objecttype_attributes?uuid=' + mapping.objecttype_id;
            } else {
              return 'attributes';
            }
          }

          return restrict;
        };

        loadData();
      },
    ]);
})();
