// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.MessageBoxController', [
    '$scope',
    '$rootScope',
    '$q',
    '$http',
    'smartHttp',
    'translationService',
    'systemMessageService',
    'snackbarService',
    'messageBoxService',
    function (
      $scope,
      $rootScope,
      $q,
      $http,
      smartHttp,
      translationService,
      systemMessageService,
      snackbarService,
      messageBoxService
    ) {
      var maxKbAllowed = 250;
      var maxBytesAllowed = 256000;
      var maxNumFiles = 2;
      var allowedExtensions = ['pdf'];
      var loading = false;

      $scope.init = function () {
        validateAttachments();
      };

      var validateAttachments = function () {
        if (!fileSizesAreValid()) {
          $scope.addError(
            'files',
            'De totale bestandsgrootte mag niet boven de ' +
              maxKbAllowed +
              ' kB uitkomen.'
          );
        }
        if (!fileTypesAreValid()) {
          var allowed = _.reduce(
            allowedExtensions,
            function (acc, extension, i) {
              return acc + (i != 0 ? ', ' : '') + '.' + extension;
            },
            ''
          );
          $scope.addError(
            'files',
            'Er zijn één of meer ongeldige bestanden geselecteerd. Toegestaan: ' +
              allowed +
              '.'
          );
        }
        if (!fileCountIsValid()) {
          $scope.addError(
            'files',
            'Het is niet toegestaan meer dan ' +
              maxNumFiles +
              ' bestanden te versturen naar de Berichtenbox.'
          );
        }
      };

      var fileSizesAreValid = function () {
        if (!$scope.attachments || $scope.attachments.length == 0) return false;
        var total = _.reduce(
          $scope.attachments,
          function (total, file) {
            return total + file.filestore_id.size;
          },
          0
        );
        return total <= maxBytesAllowed;
      };

      var fileTypesAreValid = function () {
        if (!$scope.attachments || $scope.attachments.length == 0) return false;
        var bannedFiles = _.filter($scope.attachments, function (file, i) {
          return (
            _.indexOf(
              allowedExtensions,
              file.extension_dotless.toLowerCase()
            ) == -1
          );
        });
        return bannedFiles.length == 0;
      };

      var fileCountIsValid = function () {
        return $scope.attachments.length <= maxNumFiles;
      };

      $scope.maySubmit = function () {
        return (
          Boolean($scope.content) &&
          Boolean($scope.subject) &&
          fileSizesAreValid() &&
          fileTypesAreValid() &&
          fileCountIsValid()
        );
      };

      $scope.submit = function () {
        $scope.loading = true;

        snackbarService
          .wait('Bezig met versturen...', {
            promise: messageBoxService.submit(
              $scope.interfaceUuid,
              $scope.requestor.uuid,
              {
                recipient_type: 'aanvrager',
                log_error: 0,
                case_id: $scope.case.instance.number,
                subject: $scope.subject,
                body: $scope.content,
                file_attachments: _.map($scope.attachments, function (file, i) {
                  return file.id;
                }),
              }
            ),
            then: function (response) {
              return 'Het bericht is verzonden naar de Berichtenbox van de aanvrager.';
            },
            catch: function (error) {
              if (_.get(error, 'data.result.instance.message')) {
                return _.get(error, 'data.result.instance.message');
              } else if (_.get(error, 'data.result.preview')) {
                return _.get(error, 'data.result.preview');
              } else if (typeof error === 'string') {
                return error;
              } else {
                return 'Er is iets fout gegaan bij het versturen van het bericht. Neem contact op met de beheerder';
              }
            },
          })
          .then(function () {
            $scope.closePopup();
          })
          .finally(function () {
            $scope.loading = false;
          });
      };
    },
  ]);
