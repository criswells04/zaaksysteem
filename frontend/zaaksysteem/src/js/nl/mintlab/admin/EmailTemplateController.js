// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.EmailTemplateController', [
      '$scope',
      '$window',
      'smartHttp',
      'translationService',
      function ($scope, $window, smartHttp, translationService) {
        var indexOf = _.indexOf,
          sortBy = _.sortBy,
          catsById = {};

        $scope.categories = [];
        $scope.attachments = [];

        $scope.reloadData = function () {
          smartHttp
            .connect({
              method: 'GET',
              url: 'beheer/bibliotheek/categories',
            })
            .success(function onSuccess(data) {
              var cats = sortBy(data.result, function (cat) {
                  return cat.naam.toLowerCase();
                }),
                cat,
                i,
                l,
                ordered = [];

              function addToParent(child, parent) {
                if (parent) {
                  parent = catsById[parent.id];
                  if (!parent.children) {
                    parent.children = [];
                  }
                  parent.children.push(child);
                }
              }

              function addToOrderedList(child, depth) {
                var i,
                  l,
                  children = child.children;

                child.depth = depth;

                ordered.push(child);

                for (i = 0, l = children ? children.length : 0; i < l; ++i) {
                  addToOrderedList(children[i], depth + 1);
                }
              }

              for (i = 0, l = cats.length; i < l; ++i) {
                cat = cats[i];
                catsById[cat.id] = cat;
              }

              for (i = 0, l = cats.length; i < l; ++i) {
                cat = cats[i];
                addToParent(cat, cat.pid);
              }

              for (i = 0, l = cats.length; i < l; ++i) {
                cat = cats[i];
                if (!cat.pid) {
                  addToOrderedList(cat, 0);
                }
              }

              $scope.categories = ordered;
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Documentcategorieën konden niet worden geladen'
                ),
              });
            });

          if ($scope.itemId) {
            smartHttp
              .connect({
                method: 'GET',
                url: 'beheer/bibliotheek/notificaties/get/' + $scope.itemId,
              })
              .success(function onSuccess(data) {
                var templateData = data.result[0];

                $scope.label = templateData.label;
                $scope.subject = templateData.subject;
                $scope.message = templateData.message;
                $scope.category = templateData.bibliotheek_categorie_id.id;
                $scope.attachments = templateData.attachments;
                $scope.sender = templateData.sender;
                $scope.senderAddress = templateData.sender_address;
              })
              .error(function onError(/*data*/) {
                $scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get(
                    'Data sjabloon kon niet worden geladen'
                  ),
                });
                $scope.closePopup();
              });
          }
        };

        $scope.saveTemplate = function () {
          var id = $scope.itemId,
            update = id !== 0 ? 1 : 0,
            attachments = [],
            caseDoc;

          _.each($scope.attachments, function (caseDoc) {
            if (caseDoc.id !== undefined) {
              attachments.push(caseDoc.id);
            } else {
              attachments.push(caseDoc.bibliotheek_kenmerk_id);
            }
          });

          smartHttp
            .connect({
              method: 'POST',
              url:
                'beheer/bibliotheek/notificaties/' +
                $scope.itemId +
                '/bewerken/' +
                $scope.category,
              data: {
                bibliotheek_notificatie_id: id,
                update: update,
                label: $scope.label,
                subject: $scope.subject,
                message: $scope.message,
                sender: $scope.sender,
                sender_address: $scope.senderAddress,
                bibliotheek_categorie_id: $scope.category,
                commit_message: $scope.commitMessage,
                attachments: attachments,
              },
            })
            .success(function onSuccess(/*data*/) {
              $window.location.reload();
              $scope.closePopup();
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Sjabloon kon niet worden opgeslagen'
                ),
              });
            });
        };

        $scope.detachCaseDoc = function (caseDoc) {
          var index = indexOf($scope.attachments, caseDoc);
          if (index !== -1) {
            $scope.attachments.splice(index, 1);
          }
        };

        $scope.getCaseDocLabel = function (caseDoc) {
          var label = '';

          if (caseDoc) {
            label = caseDoc.naam || caseDoc.naam;
          }
          return label;
        };

        $scope.init = function () {
          if ($scope.itemId === 0) {
            $scope.commitMessage = translationService.get('Aanmaken');
          } else {
            $scope.commitMessage = translationService.get('Wijzigen');
          }

          $scope.category = parseInt($scope.category, 10);

          $scope.reloadData();
        };

        $scope.isAttached = function (caseDoc) {
          var i,
            l,
            attachments = $scope.attachments;

          for (i = 0, l = attachments.length; i < l; ++i) {
            if (attachments[i].id === caseDoc.id) {
              return true;
            }
          }

          return false;
        };

        $scope.getCategoryLabel = function (cat) {
          var label = cat.naam,
            prefix = '';

          if (cat.depth) {
            prefix = new Array(cat.depth + 1).join('--') + ' ';
          }
          return prefix + label;
        };

        $scope.$watch('caseDoc', function (nw /*, old*/) {
          if (nw) {
            if (!$scope.isAttached(nw)) {
              $scope.attachments.push(nw);
            }
            $scope.caseDoc = null;
          }
        });
      },
    ]);
})();
