// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.casetype')
    .controller('nl.mintlab.admin.casetype.MiddlePhasesController', [
      '$scope',
      function ($scope) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        $scope.$on('confirm.cancel', function () {
          safeApply($scope.$parent, function () {
            $scope.relation.middle_phases = false;
          });
        });
      },
    ]);
})();
