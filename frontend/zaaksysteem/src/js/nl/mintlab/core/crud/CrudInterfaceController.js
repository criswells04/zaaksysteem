// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.core.crud.CrudInterfaceController', [
      '$scope',
      '$parse',
      '$interpolate',
      'smartHttp',
      'translationService',
      '$window',
      function (
        $scope,
        $parse,
        $interpolate,
        smartHttp,
        translationService,
        $window
      ) {
        var indexOf = _.indexOf,
          forEach = _.forEach,
          buildUrl = window.zsFetch('nl.mintlab.utils.buildUrl');

        $scope.selectedItems = [];

        function applySelected(selected) {
          $scope.selectedItems = selected;
          $scope.zsCrud.setSelection(selected);
        }

        function getItemId(item) {
          return $scope.zsCrud.resolveItemId(item);
        }

        function getItemById(id) {
          var items = $scope.items || [],
            i,
            l,
            item;

          for (i = 0, l = items.length; i < l; ++i) {
            item = items[i];
            if (getItemId(item) === id) {
              return item;
            }
          }

          return null;
        }

        $scope.isAllSelected = function () {
          return (
            $scope.selectedItems.length &&
            $scope.items &&
            $scope.selectedItems.length === $scope.items.length
          );
        };

        $scope.isSelected = function (item) {
          return indexOf($scope.selectedItems, item) !== -1;
        };

        $scope.handleSelectAllClick = function (/*event*/) {
          if ($scope.isAllSelected()) {
            $scope.deselectAll();
          } else {
            $scope.selectAll();
          }
        };

        $scope.selectAll = function () {
          var items = $scope.items || [];

          $scope.selectedItems.length = 0;
          $scope.selectedItems.push.apply($scope.selectedItems, items);
          applySelected($scope.selectedItems);
        };

        $scope.deselectAll = function () {
          $scope.selectedItems.length = 0;
        };

        $scope.deselectItem = function (item) {
          var index = indexOf($scope.selectedItems, item);
          if (index !== -1) {
            $scope.selectedItems.splice(index, 1);
          }
        };

        $scope.deleteItems = function () {
          var selected = $scope.selectedItems.concat();
          forEach(selected, function (item /*, key*/) {
            $scope.deselectItem(item);
            $scope.items.splice(indexOf($scope.items, item));
          });
        };

        $scope.hasVisibleActions = function (actions) {
          var action, visible;

          if (actions === undefined) {
            actions = $scope.actions;
          }

          if (!$scope.selectedItems.length) {
            return false;
          }

          for (var i = 0, l = actions.length; i < l; ++i) {
            action = actions[i];
            visible = $scope.isVisible(action);
            if (visible) {
              return true;
            }
          }

          return false;
        };

        $scope.handleActionClick = function (action, $event) {
          var params = {
              selection_type: $scope.zsCrud.getSelectionType(),
            },
            ids = [],
            i,
            l,
            items = $scope.selectedItems.concat(),
            item,
            filterVals = $scope.getFilterValues(),
            url;

          for (i = 0, l = items.length; i < l; ++i) {
            item = items[i];
            ids.push(getItemId(item));
          }

          for (var key in filterVals) {
            params[key] = filterVals[key];
          }

          if ($scope.selectedItems[0].object_type !== 'case') {
            params.selection_id = ids;
          }

          switch (action.type) {
            case 'click':
              $parse(action.data.click)($scope, {
                $selection: $scope.selectedItems,
                $event: $event,
              });
              break;

            case 'download':
              if (action.data && action.data.url) {
                if (action.data.params) {
                  params = _.merge(params, action.data.params);
                }
                url = buildUrl($interpolate(action.data.url)($scope), params);

                smartHttp
                  .connect({
                    method: 'GET',
                    url: url,
                  })
                  .success(function (data) {
                    $scope.$emit('systemMessage', {
                      type: 'info',
                      content: translationService.get(
                        'Exportbestand wordt gegenereerd. Er wordt na afronding een bericht verstuurd.'
                      ),
                    });
                  })
                  .error(function () {
                    $scope.$emit('systemMessage', {
                      type: 'error',
                      content: translationService.get(
                        'Er ging iets fout bij het initiëren van de generatie van het exportbestand.'
                      ),
                    });
                  });
              }
              break;

            case 'update':
              smartHttp
                .connect({
                  method: 'POST',
                  url: action.data.url,
                  data: params,
                })
                .success(function (data) {
                  var updated = data.result,
                    i,
                    l,
                    update,
                    item;

                  for (i = 0, l = updated.length; i < l; ++i) {
                    update = updated[i];
                    item = getItemById(getItemId(update));
                    if (item) {
                      for (var key in update) {
                        item[key] = update[key];
                      }
                    }
                  }
                })
                .success(function (/*data*/) {
                  $scope.$emit('systemMessage', {
                    type: 'info',
                    content: translationService.get(
                      'Actie succesvol uitgevoerd.'
                    ),
                  });

                  $scope.reloadData();
                })
                .error(function () {
                  $scope.$emit('systemMessage', {
                    type: 'error',
                    content: translationService.get(
                      'Er ging iets fout bij het wijzigen van de geselecteerde items'
                    ),
                  });
                });
              break;

            case 'delete':
              while (items.length) {
                var index;
                item = items.shift();
                index = indexOf($scope.selectedItems, item);
                if (index !== -1) {
                  $scope.selectedItems.splice(index, 1);
                }
                index = indexOf($scope.items, item);
                if (index !== -1) {
                  $scope.items.splice(index, 1);
                }
              }

              smartHttp
                .connect({
                  method: 'POST',
                  url: action.data.url,
                  data: params,
                })
                .success(function (/*data*/) {
                  $scope.$emit('systemMessage', {
                    type: 'info',
                    content: translationService.get(
                      'Items succesvol in de verwijderwachtrij geplaatst'
                    ),
                  });

                  $scope.reloadData();
                })
                .error(function (/*data*/) {
                  $scope.$emit('systemMessage', {
                    type: 'error',
                    content: translationService.get(
                      'Niet alle items konden worden verwijderd'
                    ),
                  });

                  $scope.reloadData();
                });
              break;
          }

          $scope.$broadcast('zs.crud.action.click', action);

          $event.stopPropagation();
        };

        $scope.handleConfirmClick = function (action, $event) {
          var click = action.data.click;

          $parse(click)($scope, {
            $event: $event,
            $selection: $scope.selectedItems,
          });
        };

        $scope.getItemStyle = function (item) {
          var obj = {},
            classes;

          if ($scope.style && $scope.style.classes) {
            classes = $scope.style.classes;
            for (var key in classes) {
              obj[key] = !!$parse(classes[key])(item);
            }
          }

          return obj;
        };

        $scope.getUrl = function (item) {
          var url = '';
          if ($scope.options && $scope.options.link) {
            url = $interpolate($scope.options.link)(item);
          }
          return url;
        };

        $scope.setSelection = function (sel) {
          $scope.selectedItems.length = 0;
          $scope.selectedItems.push.apply($scope.selectedItems, sel);

          applySelected($scope.selectedItems);
        };

        $scope.handleActionMenuClick = function (item, $event) {
          $scope.setSelection([item]);

          $event.stopPropagation();
        };

        $scope.isVisible = function (action) {
          var isVisible =
            action.when === undefined || action.when === null
              ? true
              : $scope.$eval(action.when);
          return isVisible;
        };

        $scope.$watch(
          'selectedItems',
          function () {
            if (
              $scope.zsCrud.getSelectionType() === 'all' &&
              !$scope.isAllSelected()
            ) {
              $scope.setSelectionType('subset');
            }
          },
          true
        );

        $scope.$on('zsSelectableList:change', function (event, selected) {
          if (!$scope.$$phase && !$scope.$root.$$phase) {
            $scope.$apply(function () {
              applySelected(selected);
            });
          } else {
            applySelected(selected);
          }
          event.stopPropagation();
        });

        $scope.$watch('items', function () {
          $scope.selectedItems.length = 0;
        });
      },
    ]);
})();
