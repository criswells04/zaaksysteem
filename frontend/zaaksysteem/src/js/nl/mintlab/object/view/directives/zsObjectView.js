// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.object.view').directive('zsObjectView', [
    '$q',
    'objectService',
    'zqlService',
    'systemMessageService',
    'translationService',
    'zqlEscapeFilter',
    function (
      $q,
      objectService,
      zqlService,
      systemMessageService,
      translationService,
      zqlEscapeFilter
    ) {
      return {
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this,
              object,
              activeTab,
              loadingDeferred,
              objectType,
              numLocationAttrs = 0,
              activeMarkerCoords,
              loading;

            function getObjectTypePrefix() {
              return object ? object.type : undefined;
            }

            function setTabs() {
              var type = getObjectTypePrefix(),
                tabs;
              switch (type) {
                default:
                  tabs = [
                    {
                      id: 'fields',
                      label: translationService.get('Gegevens'),
                      templateUrl: '/html/object/view/fields.html',
                    },
                    {
                      id: 'timeline',
                      label: translationService.get('Timeline'),
                      templateUrl: '/html/object/view/timeline.html',
                    },
                  ];

                  if (numLocationAttrs) {
                    tabs.push({
                      id: 'location',
                      label:
                        translationService.get('Locatie') +
                        ' (' +
                        numLocationAttrs +
                        ')',
                      templateUrl: '/html/object/view/location.html',
                    });
                  }
                  break;
              }

              ctrl.tabs = tabs;
              activeTab = ctrl.tabs[0];
            }

            ctrl.getObjectTitle = function () {
              return object ? object.label : undefined;
            };

            ctrl.getObjectTypeLabel = function () {
              return objectType ? objectType.label : undefined;
            };

            ctrl.isActiveTab = function (tab) {
              return activeTab === tab;
            };

            ctrl.selectTab = function (tab) {
              activeMarkerCoords = null;
              activeTab = tab;
            };

            ctrl.gotoLocation = function (data) {
              ctrl.selectTab(_.last(ctrl.tabs));
              activeMarkerCoords = data;
            };

            ctrl.getActiveTab = function () {
              return activeTab;
            };

            ctrl.getActiveTabPartial = function () {
              return activeTab ? activeTab.templateUrl : undefined;
            };

            ctrl.convertGeoForDisplay = function (coords) {
              return (
                '<span class="label">Latitude: </span><span class="value">' +
                Number(coords[0]).toFixed(6) +
                '</span>, ' +
                '<span class="label">Longitude: </span><span class="value">' +
                Number(coords[1]).toFixed(6) +
                '</span>'
              );
            };

            ctrl.getMapMarkerData = function () {
              var obj = ctrl.getObject();

              return _.filter(objectType.values.attributes, function (attr) {
                return attr.attribute_type === 'geolatlon';
              })
                .map(function (value) {
                  return _.last(value.name.split('.'));
                })
                .filter(function (item) {
                  return obj.values[item];
                })
                .map(function (geoValueKey) {
                  var coordsString = obj.values[geoValueKey],
                    coords = coordsString.replace(/ /g, '').split(','),
                    label = _.find(objectType.values.attributes, {
                      name: 'attribute.' + geoValueKey,
                    }).label;

                  return {
                    activeMarker: coordsString === activeMarkerCoords,
                    coordinate: coords,
                    popupData:
                      '<ul class="map-balloon-list"><li><div class="map-balloon-titel">Kenmerk: </div><div>' +
                      label +
                      '</div></li><li><div class="map-balloon-titel">Coördinaten: </div><div>' +
                      ctrl.convertGeoForDisplay(
                        coordsString.replace(/ /g, '').split(',')
                      ) +
                      '</div></li></ul>',
                  };
                });
            };

            ctrl.getObject = function () {
              return object;
            };

            ctrl.getObjectType = function () {
              return objectType;
            };

            ctrl.loadObject = function () {
              return loadingDeferred.promise;
            };

            ctrl.getInclude = function () {
              var url;

              if (!loading) {
                switch (getObjectTypePrefix()) {
                  default:
                    url = '/html/object/view/generic.html';
                    break;
                }
              }

              return url;
            };

            ctrl.isLoading = function () {
              return loading;
            };

            ctrl.onUpdateListeners = [];

            ctrl.tabs = [];

            ctrl.loadObjectDescriptionData = function () {
              return zqlService
                .query(
                  'SELECT WITH DESCRIPTION {} FROM ' +
                    object.type +
                    ' WHERE object.uuid = ' +
                    zqlEscapeFilter(object.id)
                )
                .success(function (response) {
                  numLocationAttrs = _.filter(
                    response.result[0].describe,
                    function (description) {
                      return (
                        description.attribute_type === 'geolatlon' &&
                        description.human_value
                      );
                    }
                  ).length;

                  setTabs();
                })
                .error(function (/*response*/) {
                  systemMessageService.emitLoadError('de gegevens');
                });
            };

            loading = true;

            loadingDeferred = $q.defer();

            objectService
              .getObject($attrs.objectId, { deep_relations: true })
              .then(function (obj) {
                object = obj;
                zqlService
                  .query(
                    'SELECT {} FROM type WHERE prefix = ' +
                      zqlService.escape(getObjectTypePrefix()),
                    { deep_relations: true }
                  )
                  .success(function (response) {
                    objectType = response.result[0];
                    if (
                      !objectType &&
                      _.indexOf(['faq', 'product'], getObjectTypePrefix()) ===
                        -1
                    ) {
                      loadingDeferred.reject();
                    } else {
                      loadingDeferred.resolve();
                    }
                  })
                  .error(function (response) {
                    loadingDeferred.reject(response);
                  });
              })
              ['catch'](function (obj) {
                loadingDeferred.reject(obj);
              });

            loadingDeferred.promise
              .then(function () {
                setTabs();
                _.each(ctrl.onUpdateListeners, function (listener) {
                  listener();
                });
              })
              ['catch'](function () {
                systemMessageService.emitLoadError('het object');
              })
              ['finally'](function () {
                loading = false;
              });

            return ctrl;
          },
        ],
        controllerAs: 'objectView',
      };
    },
  ]);
})();
