// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  var dependencies = [
    'ngRoute',
    'ngAnimate',
    'LocalStorageModule',
    'MintJS',
    'Zaaksysteem.admin',
    'Zaaksysteem.api',
    'Zaaksysteem.auth',
    'Zaaksysteem.case',
    'Zaaksysteem.core',
    'Zaaksysteem.docs',
    'Zaaksysteem.form',
    'Zaaksysteem.kcc',
    'Zaaksysteem.menu',
    'Zaaksysteem.message',
    'Zaaksysteem.net',
    'Zaaksysteem.notification',
    'Zaaksysteem.object',
    'Zaaksysteem.pip',
    'Zaaksysteem.subject',
    'Zaaksysteem.sysin',
    'Zaaksysteem.timeline',
    'Zaaksysteem.user',
    'Zaaksysteem.ui',
    'Zaaksysteem.widget',
    'Zaaksysteem.migrate',
    'Zaaksysteem.directives',
    'Zaaksysteem.filters',
  ];

  // Legacy pages that are not iframed need the
  // monkey-patched /frontend webpack bundle with
  // the Appbar/Drawer/FAB layout from /client.
  if (window.parent === window) {
    dependencies.push('zs-new');
  } else {
    dependencies.push('zs-new-abstract');
  }

  angular
    .module('Zaaksysteem', dependencies)
    .config([
      '$httpProvider',
      '$interpolateProvider',
      'smartHttpProvider',
      '$anchorScrollProvider',
      'localStorageServiceProvider',
      function (
        $httpProvider,
        $interpolateProvider,
        smartHttpProvider,
        $anchorScrollProvider,
        localStorageServiceProvider
      ) {
        $interpolateProvider.startSymbol('<[');
        $interpolateProvider.endSymbol(']>');
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
        $anchorScrollProvider.disableAutoScrolling();

        var prefix = '/';
        var loc = window.location.href;

        if (loc.match(/^http(s):\/\/localhost/)) {
          prefix = 'http://sprint.zaaksysteem.nl/';
        }

        smartHttpProvider.defaults.prefix = prefix;
        localStorageServiceProvider.setPrefix('zaaksysteem');
        $httpProvider.interceptors.push(function () {
          return {
            request: function (config) {
              var key;
              var params;
              var val;

              if (
                config.method &&
                config.method.toUpperCase() === 'GET' &&
                config.params
              ) {
                params = config.params;

                for (key in params) {
                  val = params[key];

                  if (val === true || val === false) {
                    params[key] = val ? 1 : 0;
                  }
                }
              }

              return config;
            },
          };
        });
      },
    ])
    // turn off angular routing
    .config([
      '$provide',
      function ($provide) {
        $provide.decorator('$browser', [
          '$delegate',
          function ($delegate) {
            $delegate.onUrlChange = function () {};
            $delegate.url = function () {
              return '';
            };

            return $delegate;
          },
        ]);
      },
    ])
    .run([
      '$rootScope',
      '$compile',
      '$cookies',
      '$document',
      '$window',
      function ($rootScope, $compile, $cookies, $document, $window) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        $rootScope.$on('legacyDomLoad', function (event, jq) {
          safeApply($rootScope, function () {
            var i;
            var l;
            var el;
            var scope;

            for (i = 0, l = jq.length; i < l; ++i) {
              el = jq[i];
              scope = angular.element(el).scope();
              $compile(el)(scope);
            }
          });
        });

        // FIXME(dario): this is very unclean, perhaps a define/fetch() method?
        window.getXSRFToken = function () {
          return $cookies.get('XSRF-TOKEN');
        };

        if ($cookies.get('XSRF-TOKEN')) {
          $.ajaxSetup({
            headers: { 'X-XSRF-TOKEN': $cookies.get('XSRF-TOKEN') },
          });
        }

        $($document[0]).ajaxSend(function (event, xhr) {
          var accessToken = $cookies['XSRF-TOKEN'];

          xhr.setRequestHeader('X-XSRF-TOKEN', accessToken);
        });
      },
    ])
    .run([
      '$window',
      '$document',
      function ($window, $document) {
        if ($window.location.href.indexOf('/beheer') !== -1) {
          $document.find('body').addClass('view-beheer');
        }
      },
    ]);
})();
