// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.widget.search').directive('zsSearchColumnList', [
    function () {
      return {
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this;

            ctrl.getItems = function () {
              var list = $scope.$eval($attrs.list),
                items;

              if (_.isArray(list)) {
                items = list;
              } else if (list) {
                items = [list];
              }

              return items;
            };

            return ctrl;
          },
        ],
        controllerAs: 'searchColumnList',
      };
    },
  ]);
})();
