// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.widget.search').directive('zsAttributeEditForm', [
    function () {
      return {
        require: '^?zsAttributeFormField',
        scope: true,
        link: function (scope, element, attrs, zsAttributeFormField) {
          scope.$on('form.change.committed', function () {
            zsAttributeFormField.saveList();
          });
        },
      };
    },
  ]);
})();
