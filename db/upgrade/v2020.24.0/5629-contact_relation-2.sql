BEGIN;
    ALTER TABLE zaak_betrokkenen DROP CONSTRAINT zaak_betrokkenen_bibliotheek_kenmerken_id_fkey;
    ALTER TABLE zaak_betrokkenen ADD CONSTRAINT zaak_betrokkenen_bibliotheek_kenmerken_id_fkey
        FOREIGN KEY(bibliotheek_kenmerken_id) REFERENCES bibliotheek_kenmerken(id) ON DELETE SET NULL;
COMMIT;
