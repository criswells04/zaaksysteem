BEGIN;

ALTER TABLE transaction ADD COLUMN processor_params TEXT;
ALTER TABLE transaction ADD COLUMN error_fatal BOOLEAN;

COMMIT;