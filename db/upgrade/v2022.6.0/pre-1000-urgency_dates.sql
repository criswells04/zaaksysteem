BEGIN;

    ALTER TABLE zaak ADD urgency_date_medium TIMESTAMP WITHOUT TIME ZONE GENERATED ALWAYS AS ((streefafhandeldatum - registratiedatum) * 0.8 + registratiedatum) STORED;
    ALTER TABLE zaak ADD urgency_date_high TIMESTAMP WITHOUT TIME ZONE GENERATED ALWAYS AS ((streefafhandeldatum - registratiedatum) * 0.9 + registratiedatum) STORED;

    DROP INDEX IF EXISTS zaak_urgency_medium_idx;
    CREATE INDEX zaak_urgency_medium_idx ON zaak(urgency_date_medium) WHERE status != 'deleted' AND deleted IS NULL;
    DROP INDEX IF EXISTS zaak_urgency_high_idx;
    CREATE INDEX zaak_urgency_high_idx ON zaak(urgency_date_high) WHERE status != 'deleted' AND deleted IS NULL;
    DROP INDEX IF EXISTS zaak_streefandeldatum_idx;
    CREATE INDEX zaak_streefandeldatum_idx ON zaak(streefafhandeldatum) WHERE status != 'deleted' AND deleted IS NULL;

COMMIT;
