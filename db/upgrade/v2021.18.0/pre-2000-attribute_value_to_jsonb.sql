BEGIN;

  CREATE OR REPLACE FUNCTION bag_type_id_to_json(
    IN btype text,
    IN bid text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    val text;

    bag_record jsonb;
    bag_json jsonb;

    display_name text;
    lat_long text;
  BEGIN

    bid := lpad(bid, 16, '0');

    SELECT INTO bag_record bag_data FROM bag_cache bc WHERE
      bc.bag_type = btype and bc.bag_id = bid;

    SELECT INTO lat_long REGEXP_REPLACE(bag_record->>'centroide_ll','POINT\((.*) (.*)\)','\2,\1');

    SELECT INTO bag_json json_build_object(
        'identification', bid
    );

    IF btype = 'nummeraanduiding'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long,
        'huisnummer', bag_record->'huisnummer',
        'postcode', bag_record->'postcode',
        'huisletter', bag_record->'huisletter',
        'huisnummertoevoeging', bag_record->'huisnummertoevoeging',
        'woonplaats', bag_record->'woonplaatsnaam',

        -- We don't have the data in the DB, if there.. fill them.
        -- This is just here to keep the API the same.
        'end_date', null,
        'start_date', null,
        'under_investigation', null,
        'document_date', null,
        'document_number', null
      );

    ELSIF btype = 'openbareruimte'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'straat', bag_record->'straatnaam',
        'gps_lat_lon', lat_long
      );

    ELSIF btype = 'verblijfsobject'
    THEN

      SELECT INTO bag_json bag_json || jsonb_build_object(
        'surface_area', bag_record->'wfs_data'->'oppervlakte',
        'year_of_construction', bag_record->'wfs_data'->'bouwjaar',
        'gebruiksdoel', bag_record->'wfs_data'->'gebruiksdoel'
      );

    END IF;

    display_name := bag_record->>'weergavenaam';

    select into value_json jsonb_build_object(
      'human_identifier', display_name,
      'bag_id', CONCAT(btype, '-', bid),
      'address_data', bag_json
    );

  RETURN;
  END;
  $$;


  CREATE OR REPLACE FUNCTION bag_attribute_value_to_jsonb(
    IN value text[],
    IN value_type text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    val text;

    btype text;
    bid text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));
      IF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      FOREACH val IN ARRAY value
      LOOP

        btype := split_part(val, '-', 1);
        bid   := split_part(val, '-', 2);

        SELECT INTO value_json value_json || bag_type_id_to_json(btype, bid);

      END LOOP;
      RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION attribute_value_to_jsonb(
    IN value text[],
    IN value_type text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type = 'date'
      THEN
        SELECT INTO value array_agg(attribute_date_value_to_text(value[1]));
      END IF;

      IF value_type LIKE 'bag_%'
      THEN
        SELECT INTO value bag_attribute_value_to_jsonb(value, value_type);
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);
      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;
      RETURN;
  END;
  $$;

COMMIT;

