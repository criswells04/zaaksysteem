BEGIN;

ALTER TABLE scheduled_jobs ADD COLUMN case_id integer;
CREATE INDEX scheduled_jobs_task_idx ON scheduled_jobs (task);
CREATE INDEX scheduled_jobs_case_id_idx ON scheduled_jobs (case_id);

UPDATE scheduled_jobs SET case_id=subquery.param_case_id::integer FROM (select id,substring(parameters, '"case_id":(\d+)(?:,|})') as param_case_id from scheduled_jobs where task='case/update_kenmerk') as subquery where scheduled_jobs.id=subquery.id;

COMMIT;
