BEGIN;

    UPDATE adres SET landcode = 6030 WHERE landcode IS NULL;
    ALTER TABLE adres ALTER COLUMN landcode SET NOT NULL;

    /* Changes I want to make, but not sure if it is ok */
--    UPDATE natuurlijk_persoon SET status_geheim = true WHERE status_geheim IS NULL;
--    ALTER TABLE natuurlijk_persoon ALTER COLUMN status_geheim SET NOT NULL;

COMMIT;
