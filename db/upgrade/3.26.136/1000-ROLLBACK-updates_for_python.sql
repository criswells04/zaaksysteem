BEGIN;
	-- MINTY-691
	DROP INDEX IF EXISTS bibliotheek_category_unique_name;

	-- MINTY-630
	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN help DROP NOT NULL;
	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN help SET DEFAULT NULL;

	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN value_default DROP NOT NULL;
	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN value_default SET DEFAULT NULL;

	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN naam_public DROP NOT NULL;
	ALTER TABLE bibliotheek_kenmerken ALTER COLUMN naam_public SET DEFAULT NULL;

	-- MINTY-660
	ALTER TABLE directory DROP COLUMN IF EXISTS uuid;
COMMIT;
