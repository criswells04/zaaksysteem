BEGIN;

  CREATE OR REPLACE FUNCTION subject_person_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address json;
    address_cor json;
    address_res json;

    partner json;

  BEGIN

    SELECT INTO json subject_json(subject, 'person');

    address := json_build_object(
      'preview', 'address(unsynched)',
      'reference', null,
      'type', 'address',
      'instance', json_build_object(
        'bag_id', subject->'bag_id',
        'city', subject->'city',
        'street', subject->'street',
        'street_number', (subject->'street_number')::int,
        'street_number_suffix', subject->'street_number_suffix',
        'street_number_letter', subject->'street_number_letter',
        'foreign_address_line1', subject->'foreign_address_line1',
        'foreign_address_line2', subject->'foreign_address_line2',
        'foreign_address_line3', subject->'foreign_address_line3',
        'zipcode', subject->'zipcode',
        'country', (subject->'country')::jsonb,
        'municipality', (subject->'municipality')::jsonb,
        'latitude', (subject->'latitude')::float,
        'longitude', (subject->'longitude')::float
      )
    );

    IF subject->'address_type' = 'W' THEN
          address_res := address;
    ELSE
          address_cor := address;
    END IF;

    IF   subject->'partner_a_nummer' IS NOT NULL
      OR subject->'partner_burgerservicenummer' IS NOT NULL
      OR subject->'partner_geslachtsnaam' IS NOT NULL
      OR subject->'partner_voorvoegsel' IS NOT NULL
    THEN
      SELECT INTO partner json_build_object(
        'instance', json_build_object(
          'surname', subject->'partner_geslachtsnaam',
          'family_name' , subject->'partner_geslachtsnaam',
          'prefix', subject->'partner_voorvoegsel',
          'personal_number', lpad(subject->'partner_burgerservicenummer', 9, '0'),
          'personal_number_a', lpad(subject->'partner_a_nummer', 10, '0'),
          -- We have no information on these items
          'first_names' , null,
          'use_of_name', null,
          'is_local_resident', false,
          'gender', null,
          'noble_title', null,
          'place_of_birth', null,
          'initials', null,
          'date_of_birth', null,
          'is_secret', 0,
          'email_address', null,
          'mobile_phone_number', null,
          'address_correspondence', null,
          'address_residence', null,
          'date_of_death', null,
          'phone_number', null,
          'partner', null,
          'date_created', NOW(),
          'date_modified', NOW()
        ),
        'type', 'person',
        'preview', 'person(unsynched)',
        'reference', null
      );
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'partner', partner,
        -- non-existent data
        'date_created', NOW(), -- np table?
        'date_modified', NOW(), -- np table?
        -- we have it
        'date_of_birth', (subject->'geboortedatum')::date,
        'date_of_death', (subject->'datum_overlijden')::date,
        'family_name', subject->'geslachtsnaam',
        'first_names', subject->'voornamen',
        'gender', subject->'geslachtsaanduiding',
        'initial', subject->'voorletters',
        'is_local_resident', (subject->'in_gemeente')::boolean,
        'is_secret', COALESCE((subject->'indicatie_geheim'), '0'),
        'noble_title', subject->'adellijke_titel',
        'personal_number', lpad(subject->'burgerservicenummer', 9, '0'),
        'personal_number_a', lpad(subject->'a_nummer', 10, '0'),
        'place_of_birth', subject->'geboorteplaats',
        'prefix', subject->'aanhef_aanschrijving',
        'surname', COALESCE(subject->'naamgebruik', subject->'geslachtsnaam'),
        'use_of_name', subject->'aanduiding_naamgebruik'
      )
    );

    RETURN;
  END;
  $$;

COMMIT;

