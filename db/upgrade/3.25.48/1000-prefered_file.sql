BEGIN;

    -- New entries should be explicitly set to true
    ALTER TABLE filestore ADD COLUMN is_archivable boolean default false not null;
    -- and old entries are considered prefered
    UPDATE filestore SET is_archivable = true;

COMMIT;
