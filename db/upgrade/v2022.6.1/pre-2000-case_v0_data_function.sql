BEGIN;

  CREATE OR REPLACE FUNCTION build_case_v0_values(
    z hstore,
    zm hstore,
    zt hstore,
    ztr hstore,
    zts hstore,
    zts_next hstore,
    rpt hstore,
    ztd hstore,
    gr hstore,
    ro hstore,
    case_location hstore,
    parent hstore,
    requestor hstore,
    recipient hstore,
    bc text,
    gassign text,
    ztn jsonb
  )
  RETURNS JSONB
  IMMUTABLE
  LANGUAGE plpgsql
  AS $$
  DECLARE
    case_v0 jsonb;
    mapped text;

    ztr_properties jsonb;
    assignee jsonb;
    coordinator jsonb;

  BEGIN

    ztr_properties := (ztr->'properties')::jsonb;
    assignee := (z->'assignee_v1_json')::jsonb;
    coordinator := (z->'coordinator_v1_json')::jsonb;

--    -- zs::attributes and friends
    return jsonb_build_object(
      'date_created', timestamp_to_perl_datetime(z->'created'),
      'date_modified', timestamp_to_perl_datetime(z->'last_modified'),

      'case.route_ou', (z->'route_ou')::int,
      'case.route_role', (z->'route_role')::int,

      'case.channel_of_contact', z->'contactkanaal',
      'case.html_email_template', z->'html_email_template',

      'case.current_deadline', (zm->'current_deadline')::jsonb,
      'case.deadline_timeline', (zm->'deadline_timeline')::jsonb,

      'case.date_destruction', timestamp_to_perl_datetime(z->'vernietigingsdatum'),
      'case.date_of_completion', timestamp_to_perl_datetime(z->'afhandeldatum'),
      'case.date_of_registration', timestamp_to_perl_datetime(z->'registratiedatum'),
      'case.date_of_registration_full', to_char((z->'registratiedatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY HH24:MI:SS'),
      'case.date_of_completion_full', to_char((z->'afhandeldatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY HH24:MI:SS'),

      'case.date_target', CASE WHEN z->'status' = 'stalled' THEN 'Opgeschort' ELSE timestamp_to_perl_datetime(z->'streefafhandeldatum') END,

      'case.stalled_since', timestamp_to_perl_datetime(zm->'stalled_since'),
      'case.stalled_until', timestamp_to_perl_datetime(z->'stalled_until'),

      'case.suspension_rationale', zm->'opschorten',

      'case.urgency', z->'urgency',

      'case.startdate', to_char((z->'registratiedatum')::timestamp with time zone at time zone 'Europe/Amsterdam', 'DD-MM-YYYY'),

      'case.progress_days', get_date_progress_from_case(z),
      'case.progress_status', (z->'status_percentage')::numeric,

      'case.days_left', CASE WHEN z->'status' = 'stalled' THEN NULL ELSE (z->'streefafhandeldatum')::date - COALESCE((z->'afhandeldatum')::date, NOW()::date) END,

      'case.lead_time_real', z->'leadtime',

      'case.destructable', is_destructable((z->'vernietigingsdatum')::date),

      'case.number_status', (z->'milestone')::int,
      'case.milestone', CASE WHEN zts_next->'id' IS NOT NULL THEN zts->'naam' ELSE null END,
      'case.phase', CASE WHEN zts_next->'id' IS NOT NULL THEN zts_next->'fase' ELSE null END,

      'case.status', z->'status',

      'case.subject', z->'onderwerp',
      'case.subject_external', z->'onderwerp_extern',

      'case.archival_state', z->'archival_state',
      'case.confidentiality', get_confidential_mapping(z->'confidentiality'),

      'case.payment_status', get_payment_status_mapping(z->'payment_status'),
      'case.price', z->'dutch_price',

      -- documents
      'case.documents', COALESCE((SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', ') FROM file f
        WHERE f.case_id = (z->'id')::int and f.date_deleted is null and f.active_version = true), ''),

      -- attributes with files (used in e-mail tab for JS)
      'case.case_documents', COALESCE((SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', ') FROM file case_documents
        JOIN file_case_document fcd
          ON fcd.case_id = case_documents.case_id and case_documents.id = fcd.file_id
        WHERE case_documents.case_id = (z->'id')::int), ''),

      -- cached values which potentially block phase transitions
      'case.num_unaccepted_updates', (zm->'unaccepted_attribute_update_count')::int,
      'case.num_unread_communication', (zm->'unread_communication_count')::int,
      'case.num_unaccepted_files', (zm->'unaccepted_files_count')::int,

--      -- static values
      'case.aggregation_scope', 'Dossier'
    )
    -- Case/casetype result blob
    || jsonb_build_object(
      'case.result', z->'resultaat',
      'case.result_description', ztr->'label',
      'case.result_explanation', ztr->'comments',
      'case.result_id', (ztr->'id')::int,
      'case.result_origin', ztr_properties->'herkomst',
      'case.result_process_term', ztr_properties->'procestermijn',
      'case.result_process_type_description', ztr_properties->'procestype_omschrijving',
      'case.result_process_type_explanation', ztr_properties->'procestype_toelichting',
      'case.result_process_type_generic', ztr_properties->'procestype_generiek',
      'case.result_process_type_name', ztr_properties->'procestype_naam',
      'case.result_process_type_number', ztr_properties->'procestype_nummer',
      'case.result_process_type_object', ztr_properties->'procestype_object',
      'case.result_selection_list_number', ztr_properties->'selectielijst_nummer',
      'case.retention_period_source_date', ztr->'ingang',
      'case.type_of_archiving', ztr->'archiefnominatie',
      'case.period_of_preservation', rpt->'label',
      'case.period_of_preservation_active', CASE WHEN (ztr->'trigger_archival')::boolean = true THEN 'Ja' ELSE 'Nee' END,
      'case.type_of_archiving', ztr->'archiefnominatie',
      -- TODO: Pick one
      'case.selection_list', ztr->'selectielijst',
      'case.active_selection_list', ztr->'selectielijst'
    )
    -- ztd stuff
    || jsonb_build_object(
      'case.lead_time_legal', ztd->'afhandeltermijn',
      'case.lead_time_service', ztd->'servicenorm',
      'case.principle_national', ztd->'grondslag'
    )
    -- Case.number shizzle
    || jsonb_build_object(
      'case.custom_number', CASE WHEN char_length(z->'prefix') > 0 THEN CONCAT(z->'prefix', '-', z->'id') ELSE z->'id'::text END,
      'case.number', (z->'id')::int,
      'case.number_master', (z->'number_master')::int
    )
    -- case.casetype stuff
    || jsonb_build_object(
      'case.casetype', zt->'uuid',
      'case.casetype.id', zt->'id',
      'case.casetype.generic_category', bc,
      'case.casetype.initiator_type', ztd->'handelingsinitiator',
      'case.casetype.price.web', ztd->'pdc_tarief',
      'case.casetype.process_description', ztd->'procesbeschrijving',
      'case.casetype.publicity', ztd->'openbaarheid',
      -- incorrectly named
      'case.casetype.department', gr->'name',
      'case.casetype.route_role', ro->'name'
    ) || ztn
    -- relations
    || jsonb_build_object(
      'case.parent_uuid', COALESCE(parent->'uuid', '')
    )
    || jsonb_build_object(
    -- assignee
      'assignee', assignee->>'reference',
      'case.assignee', assignee->>'preview',
      'case.assignee.uuid', assignee->>'reference',
      'case.assignee.email', assignee->'instance'->'subject'->'instance'->>'email_address',
      'case.assignee.initials', assignee->'instance'->'subject'->'instance'->>'initials',
      'case.assignee.last_name', assignee->'instance'->'subject'->'instance'->>'surname',
      'case.assignee.first_names', assignee->'instance'->'subject'->'instance'->>'first_names',
      'case.assignee.phone_number', assignee->'instance'->'subject'->'instance'->>'phone_number',
      'case.assignee.id', (split_part(assignee->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.assignee.department', gassign,
      'case.assignee.title', ''
    )
    || jsonb_build_object(
    -- coordinator
      'coordinator', coordinator->'reference',
      'case.coordinator', coordinator->'preview',
      'case.coordinator.uuid', coordinator->'reference',
      'case.coordinator.email', coordinator->'instance'->'subject'->'instance'->>'email_address',
      'case.coordinator.phone_number', coordinator->'instance'->'subject'->'instance'->>'phone_number',
      'case.coordinator.id', (split_part(coordinator->'instance'->>'old_subject_identifier', '-', 3))::int,
      'case.coordinator.title', ''
    )
    -- requestor
    || case_subject_as_v0_json(requestor, 'requestor', false)
    || case_subject_as_v0_json(requestor, 'requestor', true)

    || jsonb_build_object(
      'case.requestor.preset_client', CASE WHEN (z->'preset_client')::boolean = true THEN 'Ja' ELSE 'Nee' END
    )

    || case_subject_as_v0_json(recipient, 'recipient', false)
    || case_subject_as_v0_json(recipient, 'recipient', true)
    -- case location
    || case_location_as_v0_json(case_location)
    -- case relationships
    || jsonb_build_object(
      'case.related_cases', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
             FROM view_case_relationship rel
              WHERE (z->'id')::int = rel.case_id
              AND type NOT IN ('parent', 'child')
              ORDER BY rel.order_seq
            ) a
          ), ''),
      'case.relations', COALESCE(
          (
            SELECT string_agg(id, ', ')
            FROM (
              SELECT rel.relation_id::text AS id
              FROM view_case_relationship rel
              WHERE (z->'id')::int = rel.case_id
              ORDER BY rel.order_seq
            ) a
          ), ''),
        'case.relations_complete', CASE WHEN (zm->'relations_complete')::boolean = true THEN 'Ja' ELSE 'Nee' END
    )
    -- The actual attributes
    ||
    (
      SELECT COALESCE (
        jsonb_object_agg(concat('attribute.', ca.magic_string), ca.value ::jsonb)
        FILTER (WHERE ca.magic_string is not null),
        '{}'::jsonb) as attributes
      FROM
        case_attributes_v0 ca
      WHERE
      ca.case_id = (z->'id')::int
    );

  END;
  $$;

COMMIT;

