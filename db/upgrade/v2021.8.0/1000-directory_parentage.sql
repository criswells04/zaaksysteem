BEGIN;

ALTER TABLE directory DROP CONSTRAINT IF EXISTS directory_no_self_parent;
ALTER TABLE directory ADD CONSTRAINT directory_no_self_parent CHECK(NOT (ARRAY["id"] <@ "path"));

COMMIT;
