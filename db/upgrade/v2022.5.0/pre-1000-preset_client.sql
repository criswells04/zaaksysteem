BEGIN;
    UPDATE zaaktype_definitie SET preset_client = NULL WHERE preset_client IN ('betrokkene--', '');

    ALTER TABLE zaaktype_definitie
        ADD CONSTRAINT check_preset_client
            CHECK (
                preset_client IS NULL
                OR preset_client ~ '^betrokkene-(natuurlijk_persoon|bedrijf|medewerker)-([0-9]+)$'
            );
COMMIT;
