BEGIN;

  ALTER TABLE zaak ADD COLUMN dutch_price text GENERATED ALWAYS AS (
    COALESCE(REPLACE(payment_amount::text, '.', ','), '-')
  ) STORED
  ;

COMMIT;
