BEGIN;

  DROP VIEW IF EXISTS case_v0;

  CREATE VIEW case_v0 AS
  SELECT
    z.uuid AS id,
    z.id AS object_id,

    jsonb_build_object(
      'class_uuid', null,
      'pending_changes', null
    ) as case,

    -- zs::attributes and friends
    jsonb_build_object(
      'assignee', z.assignee_v1_json->'reference',
      'case.assignee', z.assignee_v1_json->'reference',

      'coordinator', z.coordinator_v1_json->'reference',
      'case.coordinator', z.coordinator_v1_json->'reference',

      'requestor', z.requestor_v1_json->'reference',
      'case.requestor', z.requestor_v1_json->'reference',

      'case.route_ou', z.route_ou,
      'case.route_role', z.route_role,

      'case.channel_of_contact', z.contactkanaal,

      'case.date_destruction', z.vernietigingsdatum::timestamp with time zone,
      'case.date_of_completion', z.afhandeldatum::timestamp with time zone,
      'case.date_of_registration', z.registratiedatum::timestamp with time zone,
      'case.date_target', CASE WHEN status = 'stalled' THEN 'Opgeschort' ELSE to_char(z.streefafhandeldatum::timestamp with time zone at time zone 'UTC', CONCAT('YYYY-MM-DD"T"HH:MI:SS"Z"')) END,

      'case.number_status', z.milestone,
      'case.status', z.status,

      'case.suspension_rationale', CASE WHEN status = 'stalled' THEN zm.opschorten ELSE NULL END,
      'case.stalled_since', zm.stalled_since::timestamp with time zone,
      'case.stalled_until', z.stalled_until::timestamp with time zone,

      'date_created', z.created::timestamp with time zone,
      'date_modified', z.last_modified::timestamp with time zone,

      'case.deadline_timeline', zm.deadline_timeline,
      'case.current_deadline', zm.current_deadline,

      'case.urgency', z.urgency,
      'case.archival_state', z.archival_state,
      'case.confidentiality', get_confidential_mapping(z.confidentiality),

      -- case/casetype result blob
      'case.result', z.resultaat,
      'case.result_description', ztr.label,
      'case.result_explanation', ztr.comments,
      'case.result_id', ztr.id,
      'case.result_origin', ztr.properties::jsonb->'herkomst',
      'case.result_process_term', ztr.properties::jsonb->'procestermijn',
      'case.result_process_type_description', ztr.properties::jsonb->'procestype_omschrijving',
      'case.result_process_type_explanation', ztr.properties::jsonb->'procestype_toelichting',
      'case.result_process_type_generic', ztr.properties::jsonb->'procestype_generiek',
      'case.result_process_type_name', ztr.properties::jsonb->'procestype_naam',
      'case.result_process_type_number', ztr.properties::jsonb->'procestype_nummer',
      'case.result_process_type_object', ztr.properties::jsonb->'procestype_object',
      'case.result_selection_list_number', ztr.properties::jsonb->'selectielijst_nummer',
      'case.retention_period_source_date', ztr.ingang,
      'case.type_of_archiving', ztr.archiefnominatie,
      'case.period_of_preservation', ztr.bewaartermijn, -- TODO MINTY-7356
      'case.period_of_preservation_active', CASE WHEN ztr.trigger_archival = true THEN 'Ja' ELSE 'Nee' END,
      'case.type_of_archiving', ztr.archiefnominatie,
      -- TODO: Pick one
      'case.selection_list', ztr.selectielijst,
      'case.active_selection_list', ztr.selectielijst,

      -- development only
      'null', null

    ) as values,

    -- static values
    'case' as object_type,
    '{}'::text[] as related_objects

  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  ;

COMMIT;
