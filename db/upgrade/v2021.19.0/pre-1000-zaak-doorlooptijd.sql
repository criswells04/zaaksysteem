BEGIN;

  ALTER TABLE zaak ADD COLUMN leadtime INT GENERATED ALWAYS AS (

    CASE WHEN afhandeldatum IS NOT NULL THEN DATE_PART('day', afhandeldatum - registratiedatum) END

  ) STORED;

COMMIT;
