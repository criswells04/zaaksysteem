BEGIN;

ALTER TABLE zaaktype_resultaten ADD COLUMN selectielijst_brondatum DATE;
ALTER TABLE zaaktype_resultaten ADD COLUMN selectielijst_einddatum DATE;

COMMIT;
