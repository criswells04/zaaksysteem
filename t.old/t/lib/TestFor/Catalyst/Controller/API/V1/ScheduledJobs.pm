package TestFor::Catalyst::Controller::API::V1::ScheduledJobs;
use base qw(ZSTest::Catalyst);
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::ScheduledJobs - Test file for ScheduledJobs in our v1 API

=head1 SYNOPSIS

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/ScheduledJobs.pm

=head1 DESCRIPTION

This module tests the C</api/v1/scheduled_jobs> namespace.

=cut

sub api_v1_scheduled_jobs : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $mech = $zs->mech;
            $mech->zs_login;
            my $base_url = $mech->zs_url_base . '/api/v1/scheduled_job';

            with_stopped_clock(
                sub {

                    my $now = $zs->api_v1_datetime();
                    my $data = { foo => 'bar' };

                    my $sj = $zs->create_scheduled_job_ok(data => $data,);

                    {
                        my $response = $mech->get_json_ok($base_url);
                        my $type     = {
                            data            => $data,
                            date_created    => $now,
                            date_modified   => $now,
                            id              => $sj->id,
                            interface_id    => undef,
                            interval_period => undef,
                            interval_value  => undef,
                            job             => 'SomeJob',
                            next_run        => $now,
                            runs_left       => undef
                        };


                        cmp_deeply(
                            $response->{result}{instance}{rows}[0]{instance},
                            $type, "Scheduled Job is correct"
                        );
                    }

                    {
                        my $response
                            = $mech->get_json_ok("$base_url/" . $sj->id);
                        my $type = {
                            data            => $data,
                            date_created    => $now,
                            date_modified   => $now,
                            id              => $sj->id,
                            interface_id    => undef,
                            interval_period => undef,
                            interval_value  => undef,
                            job             => 'SomeJob',
                            next_run        => $now,
                            runs_left       => undef
                        };

                        cmp_deeply($response->{result}{instance},
                            $type, "Get by UUID is correct");
                    }
                }
            );

            my $naw = $zs->create_naw_ok();

            {
                my $uuid = $naw->id;
                $mech->get("$base_url/$uuid");
                $mech->validate_api_v1_error(regexp =>
                        qr/Could not find scheduled job with UUID '$uuid'/);
            }

        },
        'Get all the scheduled jobs',
    );
}

sub api_v1_scheduled_jobs_create : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $mech = $zs->mech;
            $mech->zs_login;
            my $base_url = $mech->zs_url_base . '/api/v1/scheduled_job';

            my $casetype = $zs->create_zaaktype_predefined_ok();
            my $case = $zs->create_case_ok(zaaktype => $casetype);

            my $case_uuid     = $case->object_data->id;
            my $casetype_uuid = $casetype->_sync_object->id;


            with_stopped_clock(
                sub {

                    my $next_run = $zs->api_v1_datetime(
                        DateTime->now->add('days' => 42));

                    my $now  = $zs->api_v1_datetime();
                    my %args = (
                        job             => "CreateCase",
                        next_run        => $next_run,
                        interval_value  => 1,
                        interval_period => "weeks",
                        runs_left       => 2,
                    );

                    {
                        my $response = $mech->post_json_ok(
                            "$base_url/create",
                            {
                                %args,
                                case_uuid     => $case_uuid,
                                casetype_uuid => $casetype_uuid
                            },
                            "Create scheduled job"
                        );
                        cmp_deeply(
                            $response->{result}{instance},
                            {
                                %args,
                                data          => undef,
                                date_created  => $now,
                                date_modified => $now,
                                interface_id  => undef,

                            },
                            "Scheduled Job is correct"
                        );
                        my $sj
                            = $zs->schema->resultset('ObjectRelationships')
                            ->search_rs(
                            {
                                object1_uuid =>
                                    $response->{result}{reference},
                                object1_type => $response->{result}{type}
                            }
                            );
                        my @sj = $sj->all;
                        is(@sj, 2, "Have two references");
                        is(
                            (
                                grep {
                                    $_->get_column('object2_type') eq 'case'
                                } @sj
                            ),
                            1,
                            "One is case"
                        );
                        is(
                            (
                                grep {
                                    $_->get_column('object2_type') eq
                                        'casetype'
                                } @sj
                            ),
                            1,
                            "One is casetype"
                        );
                    }

                    my $id;

                    {    # Copy relationships
                        my $response = $mech->post_json_ok(
                            "$base_url/create",
                            {
                                %args,
                                case_uuid      => $case_uuid,
                                casetype_uuid  => $casetype_uuid,
                                copy_relations => 1
                            },
                            "Create scheduled job"
                        );

                        $id = $response->{result}{reference};

                        my $sj
                            = $zs->schema->resultset('ObjectRelationships')
                            ->search_rs(
                            {
                                object1_uuid =>
                                    $response->{result}{reference},
                                object1_type => $response->{result}{type}
                            }
                            );
                        my @sj = $sj->all;
                        is(@sj, 3,
                            "Have three references because of copy relations"
                        );
                        is(
                            (
                                grep {
                                    $_->get_column('object2_type') eq 'case'
                                } @sj
                            ),
                            1,
                            "One is case"
                        );
                        is(
                            (
                                grep {
                                    $_->get_column('object2_type') eq
                                        'casetype'
                                } @sj
                            ),
                            1,
                            "One is casetype"
                        );
                        is(
                            (
                                grep {
                                    $_->get_column('object2_type') eq
                                        'scheduled_job'
                                } @sj
                            ),
                            1,
                            "One is scheduled_job"
                        );
                    }

                    {
                        my $response = $mech->post_json_ok(
                            "$base_url/$id/update",
                            {
                                job             => "CreateCase",
                                next_run        => $now,
                                interval_value  => 42,
                                interval_period => 'years',
                            },
                            "update scheduled job with $id"
                        );

                        cmp_deeply(
                            $response->{result}{instance},
                            {
                                %args,
                                data            => undef,
                                date_created    => $now,
                                date_modified   => $now,
                                interface_id    => undef,
                                next_run        => $now,
                                interval_period => 'years',
                                interval_value  => 42,
                            },
                            "Scheduled Job is correct"
                        );
                    }

                    {
                        my $response = $mech->post_json_ok(
                            "$base_url/$id/delete", {},
                            "Delete scheduled job with $id"
                        );

                        is($response->{result}{instance}{pager}{rows}, 1, "Got one scheduled job after deletion");
                    }
                }
            );
        },
        'Create scheduled case, update it and delete it',
    );
}
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
