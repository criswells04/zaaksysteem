package TestFor::General::Case::Ontvanger;

# ./zs_prove -v t/lib/TestFor/General/Case/Ontvanger.pm

use base qw(ZSTest);

use TestSetup;

sub zs_case_ontvanger : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $case = $zs->create_case_ok();
            isa_ok($case, "Zaaksysteem::Model::DB::Zaak");

            my $json_data = $case->object_data->TO_JSON;

            my $wanted = {
                'case.recipient.bsn'                  => undef,
                'case.recipient.coc'                  => undef,
                'case.recipient.country_of_birth'     => undef,
                'case.recipient.date_of_birth'        => undef,
                'case.recipient.date_of_death'        => undef,
                'case.recipient.date_of_divorce'      => undef,
                'case.recipient.date_of_marriage'     => undef,
                'case.recipient.department'           => undef,
                'case.recipient.display_name'         => undef,
                'case.recipient.email'                => undef,
                'case.recipient.establishment_number' => undef,
                'case.recipient.family_name'          => undef,
                'case.recipient.first_names'          => undef,
                'case.recipient.full_name'            => undef,
                'case.recipient.gender'               => undef,
                'case.recipient.house_number'         => undef,
                'case.recipient.id'                   => undef,

                # Is weird, should be undef
                'case.recipient.investigation'      => 'Niet in onderzoek',
                'case.recipient.mobile_number'      => undef,
                'case.recipient.naamgebruik'        => undef,
                'case.recipient.name'               => undef,
                'case.recipient.phone_number'       => undef,
                'case.recipient.place_of_birth'     => undef,
                'case.recipient.place_of_residence' => undef,
                'case.recipient.salutation'         => undef,
                'case.recipient.salutation1'        => undef,
                'case.recipient.salutation2'        => undef,

                # Is weird, should be undef
                'case.recipient.status'                  => 'Actief',
                'case.recipient.street'                  => undef,
                'case.recipient.subject_type'            => undef,
                'case.recipient.surname'                 => undef,
                'case.recipient.surname_prefix'          => undef,
                'case.recipient.trade_name'              => undef,
                'case.recipient.type'                    => undef,
                'case.recipient.type_of_business_entity' => undef,
                'case.recipient.zipcode'                 => undef,
            };

            my %res = map { $_ => $json_data->{values}{$_} } keys %$wanted;
            is_deeply(\%res, $wanted,
                "Found the correct ontvanger data with no ontvanger present");
        }
    );

    $zs->zs_transaction_ok(
        sub {

            local $Data::Dumper::Maxdepth = 3;
            my $ontvanger = $zs->create_natuurlijk_persoon_ok();

            my $case = $zs->create_case_ok(ontvanger => $ontvanger->bid,);
            isa_ok($case, "Zaaksysteem::Model::DB::Zaak");

            my $ontvanger_object = $case->zaak_betrokkenen->ontvanger;
            isnt($ontvanger_object, undef, "Ontvanger object found!");
            is($ontvanger_object->magic_string_prefix,
                'ontvanger', "Correct magic string");

            TODO: {
                local $TODO = "AUTUMN2015BREAK: Ontvanger problem";

                my $json_data = $case->object_data->TO_JSON;
                my $wanted    = {
                    'case.recipient.bsn'              => '12345678',
                    'case.recipient.coc'              => undef,
                    'case.recipient.country_of_birth' => undef,
                    'case.recipient.date_of_birth'    => undef,
                    'case.recipient.date_of_death'    => undef,
                    'case.recipient.date_of_divorce'  => undef,
                    'case.recipient.date_of_marriage' => undef,
                    'case.recipient.department'       => undef,
                    'case.recipient.display_name' => 'Anton Ano van de Zaaksysteem',
                    'case.recipient.email'        => 'devnull@zaaksysteem.nl',
                    'case.recipient.establishment_number' => undef,
                    'case.recipient.family_name'          => 'Zaaksysteem',
                    'case.recipient.first_names'          => 'Anton Ano',
                    'case.recipient.full_name'      => 'A.A. van de Zaaksysteem',
                    'case.recipient.gender'         => 'man',
                    'case.recipient.house_number'   => '42 a - 521',
                    'case.recipient.id'             => $ontvanger->bid,
                    'case.recipient.investigation'  => 'Niet in onderzoek',
                    'case.recipient.mobile_number'  => undef,
                    'case.recipient.naamgebruik'    => 'van de Zaaksysteem',
                    'case.recipient.name'           => 'A. A. van de Zaaksysteem',
                    'case.recipient.phone_number'   => undef,
                    'case.recipient.place_of_birth' => 'Polis Massa',
                    'case.recipient.place_of_residence' => 'Amsterdam',
                    'case.recipient.salutation'         => 'meneer',
                    'case.recipient.salutation1'        => 'heer',
                    'case.recipient.salutation2'        => 'de heer',
                    'case.recipient.status'             => 'Actief',
                    'case.recipient.street'             => 'Muiderstraat',
                    'case.recipient.subject_type'       => 'natuurlijk_persoon',
                    'case.recipient.surname'            => 'van de Zaaksysteem',
                    'case.recipient.surname_prefix'     => 'van de',
                    'case.recipient.trade_name'         => undef,
                    'case.recipient.type'               => 'Natuurlijk persoon',
                    'case.recipient.type_of_business_entity' => undef,
                    'case.recipient.zipcode'                 => '1011 PZ'

                };

                my %res = map { $_ => $json_data->{values}{$_} } keys %$wanted;
                is_deeply(\%res, $wanted,
                    "Found the correct ontvanger data with ontvanger present");
            };
        }
    );
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
