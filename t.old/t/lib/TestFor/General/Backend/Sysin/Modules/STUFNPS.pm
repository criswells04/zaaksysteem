package TestFor::General::Backend::Sysin::Modules::STUFNPS;

use base qw(ZSTest);
use TestSetup;

use Zaaksysteem::BR::Subject;

use constant XML_PATH => 't/inc/API/StUF/0301/prs/';

=head1 NAME

TestFor::General::Backend::Sysin::Modules::STUFNPS - StUF Koppeling Natuurlijke personen (0310)

=head1 SYNOPSIS

    See USAGE tests

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/STUFNPS.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem, based
on StUF 0301 BG 0310 and entity NPS (Natuurlijk Persoon). The first sets of tests will demonstrate
the different uses invoked from zaaksysteem. Like searching for a person and importing one.

The second part of these tests are automatic tests, which prove the different scenario's
of the system integration module with the outside. Think of messages which move a person, decease
a person etc.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 stuf_prs_usage_searching

Searching for a person.

=cut

sub opts_with_config_interface {
    my $params  = shift;

    my $cfg     = $schema->resultset('Interface')->search({ module => 'stufconfig'})->first;

    my $newp    = {
        %$params,
        config_interface_id => $cfg->id
    };

    return %$params ? $newp : %$newp;
}

sub stuf_nps_usage_searching : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {

        my $interface       = $self->_create_nps_interface();

        my $params          = {
            personal_number                             => '117859837',
            personal_number_a                           => '9856542355',
            family_name                                 => 'Fuego',
            prefix                                      => 'de',
            initials                                    => 'M',
            first_names                                 => 'Don',
            gender                                      => 'M',
            'address_residence.zipcode'                 => '1051JL',
            'address_residence.street'                  => 'Donker Curtiusstraat',
            'address_residence.street_number'           => '7',
            'address_residence.street_number_letter'    => 'a',
            'address_residence.street_number_suffix'    => '521',
            'address_residence.city'                    => 'Amsterdam',
        };
        $params = opts_with_config_interface($params);

        my $result          = $interface->process_trigger('search_nps', $params);
        isa_ok($result, 'Zaaksysteem::Model::DB::Transaction');
        ok($result->processor_params->{result}, "Got a valid result");
    }, 'Trigger: "search_nps" via Centric');
}

sub stuf_nps_usage_searching_via_bridge : Tests {
    my $self            = shift;

    my $params          = {
        subject_type    => 1,
        external_subscription => {
            external_identifier => 1,
            interface_uuid => 1,
        },
        subject         => {
            personal_number => 1,
            personal_number_a => 1,
            initials => 1,
            first_names => 1,
            family_name => 1,
            #prefix => 1,
            gender => 1,
            date_of_birth => 1,
            surname => 1,
            address_residence => {
                street => 1,
                street_number => 1,
                street_number_suffix => 1,
                street_number_letter => 1,
                zipcode => 1,
                city => 1,
                country => 1,
                #municipality => 1,
            },
            partner => {
                personal_number => 1,
                personal_number_a => 1,
                family_name => 1,
                prefix => 1,
            }
        }
    };

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_nps_interface();
        my $subject = $zs->create_subject_ok;

        my $bridge          = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            remote_search   => 'stuf',
            user => $subject,
            opts_with_config_interface({}),
        );

        my @results = $bridge->search(
            {
                'subject_type' => 'person',
                'subject.personal_number' => '117859837',
            }
        );

        $self->_verify_having_params($results[0], $params);

    }, 'Bridge: "search" and via Centric');
}

sub _verify_having_params {
    my $self        = shift;
    my $object      = shift;
    my $params      = shift;

    for my $key (keys %$params) {
        if (ref($params->{$key}) eq 'HASH') {
            $self->_verify_having_params($object->$key, $params->{$key});
            next;
        }

        ### DateTime fix
        my $value = $object->$key;
        if ($key =~ /date/ && blessed($value) && $value->isa('DateTime')) {
            $value = $value->ymd;
        }

        if ($key eq 'password') {
            ok(!$value, 'Got empty password');
            next;
        }

        ok ($value, 'Got a valid param on key: ' . $key);
    }
}

sub stuf_nps_usage_importing_via_bridge : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_nps_interface();
        my $subject = $zs->create_subject_ok();

        my $bridge          = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            remote_search   => 'stuf',
            user => $subject,
            opts_with_config_interface({}),
        );

        my @results = $bridge->search(
            {
                'subject_type' => 'person',
                'subject.personal_number' => '987654329',
            }
        );

        my $object  = $bridge->remote_import($results[0]);

        ok($object, 'Succesfully imported object');

    }, 'Bridge: "search" and "remote_import" via Centric GBA-V');

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_nps_interface();
        my $subject = $zs->create_subject_ok;

        my $bridge          = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            remote_search   => 'stuf',
            user => $subject,
            opts_with_config_interface({}),
        );

        my @results = $bridge->search(
            {
                'subject_type' => 'person',
                'subject.personal_number' => '117859837',
            }
        );

        my $object  = $bridge->remote_import($results[0]);

        ok($object, 'Succesfully imported object');

    }, 'Bridge: "search" and "remote_import" via Centric');

}

sub stuf_nps_usage_disable_subscription : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_nps_interface();
        my $subject = $zs->create_subject_ok();

        my $bridge          = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            remote_search   => 'stuf',
            user => $subject,
            opts_with_config_interface({}),
        );

        my @results = $bridge->search(
            {
                'subject_type' => 'person',
                'subject.personal_number' => '987654329',
            }
        );

        my $object  = $bridge->remote_import($results[0]);

        # ### Get subscription
        # $schema->resultset('NatuurlijkPersoon')->

        ### Object imported. Get the subscription, and disable it
        my $result  = $interface->process_trigger(
            'disable_subscription', { subscription_id => $object->external_subscription->internal_identifier }
        );


        # diag(explain($object));

    }, 'Bridge: "search" and "remote_import" via Centric');


}



=head1 HELPER METHODS

=head2 _create_prs_interface

Creates a prs interface to work with

=cut

sub _create_nps_interface {
    my $self            = shift;
    my $params          = shift || {};

    $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                stuf_supplier           => 'centric',
                synchronization_type    => 'question',
                gemeentecode            => '363',
                mk_sender               => 'ZSNL',
                mk_ontvanger            => 'CGS',
                gbav_applicatie         => 'CML',
                mk_ontvanger_afnemer    => 'CMODIS',
                mk_async_url            => 'https://localhost/stuf/async',
                mk_sync_url             => 'https://localhost/stuf/sync',
                gbav_pink_url           => 'https://localhost/stuf/pink',
                gbav_search             => 1,
                mk_spoof                => 1,
            }
        },
    );

    return $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF NPS Parsing',
            active          => 1,
        }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

