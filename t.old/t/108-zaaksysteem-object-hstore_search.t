#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Search::ZQL;

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    $case->update_hstore();

    my $rs = Zaaksysteem::Object::Model->new(schema => $schema)->rs;
    my $zql = Zaaksysteem::Search::ZQL->new('SELECT {} FROM case WHERE case.number = ' . $case->id);
    my $case_through_hstore = $zql->apply_to_resultset($rs)->single;

    is($case->id, $case_through_hstore->object_id, "Case was found through hstore search");

    my $hstore_properties = $case_through_hstore->index_hstore;

    my %object_attributes = map {
        my $name        = $_->name;
        my $index_value = $_->index_value;

        # Calling $_->index_value here breaks because of list context.
        ($name => $index_value);
    } grep {
        !$_->dynamic_class
    }@{ $case->object_attributes };

    # case->object_attributes is a separate implementation
    while (my ($k, $v) = each %object_attributes) {
        is($hstore_properties->{$k}, $v, "Object attribute $k matches hstore property value");
    }
}, 'Updating hstore_properties');

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    $case->update_hstore;

    my $rs = Zaaksysteem::Object::Model->new(schema => $schema)->rs->search({object_class => 'case'});

    is $rs->count, 1, 'ObjectData row created after $case->update_hstore';

    my $object = $rs->first;

    is $object->get_column('object_class'), 'case', 'object has proper class (' . $object->get_column('object_class') . ')';
    is $object->get_column('object_id'), $case->id, 'object has proper id (' . $object->get_column('object_id') . ')';

    $case->_delete_zaak;

    # Make sure the COUNT query is performed again
    $rs->clear_cache();
    is $rs->count, 0, 'ObjectData row deleted after $case->_delete_zaak'
        or do {
            diag explain [map { { $_->object_id => $_->object_class  } } $rs->all]
        };

}, 'Deleted cases lose ObjectData row');

zs_done_testing();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

