#!perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Test::Moose;

use Zaaksysteem::Object::Attribute;

{
    my $attr = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        value          => {
            bag_id => 'openbareruimte-1234567',
            human_identifier => 'Test Openbareruimte',
        },
        attribute_type => 'bag',
    );

    does_ok($attr, 'Zaaksysteem::Object::Attribute::BAG', 'New attribute');

    is($attr->human_value, 'Test Openbareruimte',    "Human-readable value is correct");
    is($attr->index_value, 'openbareruimte-1234567', "Indexable value is correct");
}

zs_done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

