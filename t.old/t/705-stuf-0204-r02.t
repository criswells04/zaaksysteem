#! perl
use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;

use JSON;
use Test::Deep;
use File::Spec::Functions;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    'R02'   => {
      'officieleStraatnaam' => 'Donker Curtiusstraat',
      'codeTypeOpenbareRuimte' => '01',
    }
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/r02/101-r02-create.xml'),
    );

    is($stuf->entiteittype, 'R02', 'Found entiteittype R02');

    my $params  = $stuf->as_params;
    #note(explain($params));

    for my $key (keys %{ $VALIDATION_MAP->{R02} }) {
        my $givenvalue = $params->{R02}->{'extraElementen'}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{R02}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }
    my $r02_params = $stuf->get_params_for_r02;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $r02_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );
    #note(explain($rv));

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check R02 native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/r02/101-r02-create.xml'),
    );

    my $r02_params = $stuf->get_params_for_r02;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats')->bag_create_or_update(
        $r02_params,
    );

}, 'Import R02 in database');


zs_done_testing;
